CREATE DATABASE  IF NOT EXISTS `creatio_casetest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `creatio_casetest`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: creatio_casetest
-- ------------------------------------------------------
-- Server version	5.6.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hw_spec_drive_partition`
--

DROP TABLE IF EXISTS `hw_spec_drive_partition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_drive_partition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `available_free_space` float DEFAULT NULL,
  `total_size` float DEFAULT NULL,
  `drive_format` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_drive_partition_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_drive_partition_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_drive_partition`
--

LOCK TABLES `hw_spec_drive_partition` WRITE;
/*!40000 ALTER TABLE `hw_spec_drive_partition` DISABLE KEYS */;
INSERT INTO `hw_spec_drive_partition` VALUES (1,'C:\\',27138200000,240055000000,'NTFS',6),(2,'D:\\',229112000000,640124000000,'NTFS',6),(3,'F:\\',168314000000,1000200000000,'NTFS',6),(4,'G:\\',248435000000,1000200000000,'NTFS',6),(5,'O:\\',508685000000,1000200000000,'NTFS',6),(6,'C:\\',27138200000,240055000000,'NTFS',7),(7,'D:\\',229112000000,640124000000,'NTFS',7),(8,'C:\\',27138200000,240055000000,'NTFS',9),(9,'D:\\',229112000000,640124000000,'NTFS',9),(10,'F:\\',168314000000,1000200000000,'NTFS',9),(11,'G:\\',248435000000,1000200000000,'NTFS',9),(12,'O:\\',508685000000,1000200000000,'NTFS',9);
/*!40000 ALTER TABLE `hw_spec_drive_partition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_general`
--

DROP TABLE IF EXISTS `hw_spec_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `os_name` varchar(45) DEFAULT NULL,
  `os_version` varchar(45) DEFAULT NULL,
  `os_bits` int(25) DEFAULT NULL,
  `computer_name` varchar(45) DEFAULT NULL,
  `available_physical_memory` float DEFAULT NULL,
  `available_virtual_memory` float DEFAULT NULL,
  `total_pysical_memory` float DEFAULT NULL,
  `total_virtual_memory` float DEFAULT NULL,
  `number_processors` int(11) DEFAULT NULL,
  `number_logical_processors` int(11) DEFAULT NULL,
  `pc_system_type` int(25) DEFAULT NULL,
  `win_user_name` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_general`
--

LOCK TABLES `hw_spec_general` WRITE;
/*!40000 ALTER TABLE `hw_spec_general` DISABLE KEYS */;
INSERT INTO `hw_spec_general` VALUES (1,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982300000,8793500000000,34318600000,8796090000000,1,8,1,'MasterDon',6),(2,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982300000,8793500000000,34318600000,8796090000000,1,8,1,'MasterDon',7),(3,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982300000,8793500000000,34318600000,8796090000000,1,8,1,'MasterDon',8),(4,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982300000,8793500000000,34318600000,8796090000000,1,8,1,'MasterDon',9);
/*!40000 ALTER TABLE `hw_spec_general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_hd`
--

DROP TABLE IF EXISTS `hw_spec_hd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_hd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `size` float DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `interface_type` varchar(45) DEFAULT NULL,
  `bytes_per_sector` int(25) DEFAULT NULL,
  `compression_method` varchar(45) DEFAULT NULL,
  `media_type` varchar(45) DEFAULT NULL,
  `partitions` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `total_cylinders` int(11) DEFAULT NULL,
  `total_heads` int(11) DEFAULT NULL,
  `total_sectors` int(11) DEFAULT NULL,
  `total_tracks` int(11) DEFAULT NULL,
  `tracks_per_cylinder` int(11) DEFAULT NULL,
  `default_block_size` int(25) DEFAULT NULL,
  `firmware_revision` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_hd_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_hd_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_hd`
--

LOCK TABLES `hw_spec_hd` WRITE;
/*!40000 ALTER TABLE `hw_spec_hd` DISABLE KEYS */;
INSERT INTO `hw_spec_hd` VALUES (1,'\\\\.\\PHYSICALDRIVE0','ST3640323AS ATA Device','(Standard disk drives)','Disk drive',640132000000,'2020202020202020202020205639304b42414158','IDE',512,'','Fixed hard disk media',1,'OK',77825,255,1250258625,19845375,255,0,'SD13',6),(2,'\\\\.\\PHYSICALDRIVE2','INTEL SS DSC2CW240A3 SATA Disk Device','(Standard disk drives)','Disk drive',240055000000,'CVCV303104VM240CGN  ','IDE',512,'','Fixed hard disk media',1,'OK',29185,255,468857025,7442175,255,0,'400i',6),(3,'\\\\.\\PHYSICALDRIVE3','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'S246J9HB906470      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',6),(4,'\\\\.\\PHYSICALDRIVE4','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'S246J9HB906476      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',6),(5,'\\\\.\\PHYSICALDRIVE1','ST1000NM 0011 SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'            Z1N3JETC','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'SN03',6),(6,'\\\\.\\PHYSICALDRIVE0','ST3640323AS ATA Device','(Standard disk drives)','Disk drive',640132000000,'2020202020202020202020205639304b42414158','IDE',512,'','Fixed hard disk media',1,'OK',77825,255,1250258625,19845375,255,0,'SD13',7),(7,'\\\\.\\PHYSICALDRIVE2','INTEL SS DSC2CW240A3 SATA Disk Device','(Standard disk drives)','Disk drive',240055000000,'CVCV303104VM240CGN  ','IDE',512,'','Fixed hard disk media',1,'OK',29185,255,468857025,7442175,255,0,'400i',7),(8,'\\\\.\\PHYSICALDRIVE3','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'S246J9HB906470      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',7),(9,'\\\\.\\PHYSICALDRIVE4','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'S246J9HB906476      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',7),(10,'\\\\.\\PHYSICALDRIVE1','ST1000NM 0011 SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'            Z1N3JETC','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'SN03',7),(11,'\\\\.\\PHYSICALDRIVE0','ST3640323AS ATA Device','(Standard disk drives)','Disk drive',640132000000,'2020202020202020202020205639304b42414158','IDE',512,'','Fixed hard disk media',1,'OK',77825,255,1250258625,19845375,255,0,'SD13',9),(12,'\\\\.\\PHYSICALDRIVE2','INTEL SS DSC2CW240A3 SATA Disk Device','(Standard disk drives)','Disk drive',240055000000,'CVCV303104VM240CGN  ','IDE',512,'','Fixed hard disk media',1,'OK',29185,255,468857025,7442175,255,0,'400i',9),(13,'\\\\.\\PHYSICALDRIVE3','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'S246J9HB906470      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',9),(14,'\\\\.\\PHYSICALDRIVE4','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'S246J9HB906476      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',9),(15,'\\\\.\\PHYSICALDRIVE1','ST1000NM 0011 SATA Disk Device','(Standard disk drives)','Disk drive',1000200000000,'            Z1N3JETC','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'SN03',9);
/*!40000 ALTER TABLE `hw_spec_hd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_processor`
--

DROP TABLE IF EXISTS `hw_spec_processor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_processor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `current_clock_speed` int(25) DEFAULT NULL,
  `load_percentage` int(25) DEFAULT NULL,
  `l2_cache_speed` int(25) DEFAULT NULL,
  `l3_cache_speed` int(25) DEFAULT NULL,
  `l2_cache` int(25) DEFAULT NULL,
  `l3_cache` int(25) DEFAULT NULL,
  `max_clock_speed` int(25) DEFAULT NULL,
  `number_cores` int(11) DEFAULT NULL,
  `number_logical_processors` int(11) DEFAULT NULL,
  `current_voltage` int(25) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_processor_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_processor_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_processor`
--

LOCK TABLES `hw_spec_processor` WRITE;
/*!40000 ALTER TABLE `hw_spec_processor` DISABLE KEYS */;
INSERT INTO `hw_spec_processor` VALUES (1,'AMD FX(tm)-8320 Eight-Core Processor         ','AuthenticAMD',3500,5,0,0,0,0,3500,4,8,13,6),(2,'AMD FX(tm)-8320 Eight-Core Processor         ','AuthenticAMD',3500,5,0,0,0,0,3500,4,8,13,7),(3,'AMD FX(tm)-8320 Eight-Core Processor         ','AuthenticAMD',3500,5,0,0,0,0,3500,4,8,13,9);
/*!40000 ALTER TABLE `hw_spec_processor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_ram`
--

DROP TABLE IF EXISTS `hw_spec_ram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_ram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `capacity` float DEFAULT NULL,
  `data_width` int(25) DEFAULT NULL,
  `form_factor` int(25) DEFAULT NULL,
  `interleave_data_depth` int(25) DEFAULT NULL,
  `interleave_position` int(25) DEFAULT NULL,
  `memory_type` int(25) DEFAULT NULL,
  `speed` int(25) DEFAULT NULL,
  `total_width` int(25) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_ram_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_ram_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_ram`
--

LOCK TABLES `hw_spec_ram` WRITE;
/*!40000 ALTER TABLE `hw_spec_ram` DISABLE KEYS */;
INSERT INTO `hw_spec_ram` VALUES (1,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,6),(2,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,6),(3,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,6),(4,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,6),(5,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,7),(6,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,7),(7,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,7),(8,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,7),(9,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,9),(10,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,9),(11,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,9),(12,'Physical Memory','Undefined         ','Physical Memory',8589930000,64,8,0,0,0,667,64,9);
/*!40000 ALTER TABLE `hw_spec_ram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_video`
--

DROP TABLE IF EXISTS `hw_spec_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accelerator_capabilities` varchar(45) DEFAULT NULL,
  `adapter_compatibility` varchar(45) DEFAULT NULL,
  `adapter_dac_type` varchar(45) DEFAULT NULL,
  `adapter_ram` int(11) DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `capability_descriptions` text,
  `caption` varchar(45) DEFAULT NULL,
  `color_table_entries` int(25) DEFAULT NULL,
  `config_manager_error_code` int(25) DEFAULT NULL,
  `config_manager_user_config` varchar(10) DEFAULT NULL,
  `creation_class_name` varchar(45) DEFAULT NULL,
  `current_bits_per_pixel` int(25) DEFAULT NULL,
  `current_horizontal_resolution` int(25) DEFAULT NULL,
  `current_number_colors` float DEFAULT NULL,
  `current_number_columns` int(25) DEFAULT NULL,
  `current_number_rows` int(25) DEFAULT NULL,
  `current_refresh_rate` int(25) DEFAULT NULL,
  `current_scan_mode` int(25) DEFAULT NULL,
  `current_vertical_resolution` int(25) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `device_id` varchar(45) DEFAULT NULL,
  `device_specific_pens` int(25) DEFAULT NULL,
  `dither_type` int(25) DEFAULT NULL,
  `driver_date` date DEFAULT NULL,
  `driver_version` varchar(45) DEFAULT NULL,
  `error_cleared` tinyint(1) DEFAULT NULL,
  `error_description` varchar(45) DEFAULT NULL,
  `icm_intent` int(25) DEFAULT NULL,
  `icm_method` int(25) DEFAULT NULL,
  `inf_file_name` varchar(45) DEFAULT NULL,
  `inf_section` varchar(45) DEFAULT NULL,
  `install_date` date DEFAULT NULL,
  `installed_display_drivers` text,
  `last_error_code` int(25) DEFAULT NULL,
  `max_memory_supported` int(25) DEFAULT NULL,
  `max_number_controlled` int(25) DEFAULT NULL,
  `max_refresh_rate` int(25) DEFAULT NULL,
  `min_refresh_rate` int(25) DEFAULT NULL,
  `monochrome` tinyint(1) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `number_color_planes` int(25) DEFAULT NULL,
  `number_video_pages` int(25) DEFAULT NULL,
  `pnp_device_id` varchar(90) DEFAULT NULL,
  `power_management_capabilities` int(25) DEFAULT NULL,
  `power_management_supported` tinyint(1) DEFAULT NULL,
  `protocol_supported` int(25) DEFAULT NULL,
  `reserved_system_palette_entries` int(25) DEFAULT NULL,
  `specification_version` int(25) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `status_info` varchar(45) DEFAULT NULL,
  `system_palette_entries` int(25) DEFAULT NULL,
  `time_last_reset` date DEFAULT NULL,
  `video_architecture` int(25) DEFAULT NULL,
  `video_memory_type` int(25) DEFAULT NULL,
  `video_mode` int(25) DEFAULT NULL,
  `video_mode_description` text,
  `video_processor` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_video_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_video_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_video`
--

LOCK TABLES `hw_spec_video` WRITE;
/*!40000 ALTER TABLE `hw_spec_video` DISABLE KEYS */;
INSERT INTO `hw_spec_video` VALUES (1,'0','NVIDIA','Integrated RAMDAC',1610612736,3,NULL,'NVIDIA GeForce GTX 580',0,0,'0','Win32_VideoController',32,2560,4294970000,0,0,59,4,1600,'NVIDIA GeForce GTX 580','VideoController1',0,0,'2013-09-12','9.18.13.2723',0,NULL,0,0,'oem35.inf','Section128',NULL,'nvd3dumx.dll,nvwgf2umx.dll,nvwgf2umx.dll,nvd3dum,nvwgf2um,nvwgf2um',0,0,0,75,56,0,'NVIDIA GeForce GTX 580',0,0,'PCI\\VEN_10DE&DEV_1080&SUBSYS_15803842&REV_A1\\4&2534E90F&0&0010',0,0,0,0,0,'OK','0',0,NULL,5,2,0,'2560 x 1600 x 4294967296 colors','GeForce GTX 580',6),(2,'0','NVIDIA','Integrated RAMDAC',1610612736,3,NULL,'NVIDIA GeForce GTX 580',0,0,'0','Win32_VideoController',32,2560,4294970000,0,0,59,4,1600,'NVIDIA GeForce GTX 580','VideoController1',0,0,'2013-09-12','9.18.13.2723',0,NULL,0,0,'oem35.inf','Section128',NULL,'nvd3dumx.dll,nvwgf2umx.dll,nvwgf2umx.dll,nvd3dum,nvwgf2um,nvwgf2um',0,0,0,75,56,0,'NVIDIA GeForce GTX 580',0,0,'PCI\\VEN_10DE&DEV_1080&SUBSYS_15803842&REV_A1\\4&2534E90F&0&0010',0,0,0,0,0,'OK','0',0,NULL,5,2,0,'2560 x 1600 x 4294967296 colors','GeForce GTX 580',7),(3,'0','NVIDIA','Integrated RAMDAC',1610612736,3,NULL,'NVIDIA GeForce GTX 580',0,0,'0','Win32_VideoController',32,2560,4294970000,0,0,59,4,1600,'NVIDIA GeForce GTX 580','VideoController1',0,0,'2013-09-12','9.18.13.2723',0,NULL,0,0,'oem35.inf','Section128',NULL,'nvd3dumx.dll,nvwgf2umx.dll,nvwgf2umx.dll,nvd3dum,nvwgf2um,nvwgf2um',0,0,0,75,56,0,'NVIDIA GeForce GTX 580',0,0,'PCI\\VEN_10DE&DEV_1080&SUBSYS_15803842&REV_A1\\4&2534E90F&0&0010',0,0,0,0,0,'OK','0',0,NULL,5,2,0,'2560 x 1600 x 4294967296 colors','GeForce GTX 580',9);
/*!40000 ALTER TABLE `hw_spec_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_data`
--

DROP TABLE IF EXISTS `session_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `addinVer` varchar(45) DEFAULT NULL,
  `revitBuild` varchar(45) DEFAULT NULL,
  `revitName` varchar(45) DEFAULT NULL,
  `revitLanguage` varchar(45) DEFAULT NULL,
  `session_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_data`
--

LOCK TABLES `session_data` WRITE;
/*!40000 ALTER TABLE `session_data` DISABLE KEYS */;
INSERT INTO `session_data` VALUES (5,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(6,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(7,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(8,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(9,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab');
/*!40000 ALTER TABLE `session_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timer_result`
--

DROP TABLE IF EXISTS `timer_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timer_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sequence` int(25) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `comment` text,
  `seconds` float DEFAULT NULL,
  `availPhysMem_start` float DEFAULT NULL,
  `availVirtMem_start` float DEFAULT NULL,
  `availPhysMem_end` float DEFAULT NULL,
  `availVirtMem_end` float DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_timer_result_session_data_idx` (`session_data_id`),
  CONSTRAINT `fk_timer_result_session_data` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=118 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timer_result`
--

LOCK TABLES `timer_result` WRITE;
/*!40000 ALTER TABLE `timer_result` DISABLE KEYS */;
INSERT INTO `timer_result` VALUES (1,1,'Building(s) on Level: 1','',8.1,26785900000,8794230000000,26752800000,8794220000000,6),(2,2,'Building(s) on Level: 2','',7.5,26752800000,8794220000000,26738000000,8794200000000,6),(3,3,'Building(s) on Level: 3','',8,26738000000,8794200000000,26726500000,8794200000000,6),(4,4,'Building(s) on Level: 4','',8.4,26726500000,8794200000000,26716600000,8794170000000,6),(5,5,'Building(s) on Level: 5','',8.9,26716600000,8794170000000,26701700000,8794170000000,6),(6,6,'Building(s) on Level: 6','',9.5,26701700000,8794170000000,26685100000,8794150000000,6),(7,7,'Building(s) on Level: 7','',9.8,26685100000,8794150000000,26673700000,8794130000000,6),(8,8,'Building(s) on Level: 8','',10.3,26673700000,8794130000000,26668500000,8794120000000,6),(9,9,'Building(s) on Level: 9','',10.8,26668500000,8794120000000,26655000000,8794100000000,6),(10,10,'Building(s) on Level: 10','',11.3,26655000000,8794100000000,26647100000,8794090000000,6),(11,11,'Building(s) on Level: 11','',11.9,26647100000,8794090000000,26639800000,8794090000000,6),(12,12,'Building(s) on Level: 12','',12.6,26639800000,8794090000000,26627300000,8794050000000,6),(13,13,'Building(s) on Level: 13','',12.8,26627300000,8794050000000,26610200000,8794040000000,6),(14,14,'Building(s) on Level: 14','',13.3,26610200000,8794040000000,26591500000,8794020000000,6),(15,15,'Building(s) on Level: 15','',13.8,26591500000,8794020000000,26570000000,8794020000000,6),(16,16,'Building(s) on Level: 16','',14.4,26570000000,8794020000000,26553100000,8793990000000,6),(17,17,'Building(s) on Level: 17','',15.1,26553100000,8793990000000,26532100000,8793970000000,6),(18,18,'Building(s) on Level: 18','',15.4,26532100000,8793970000000,26511400000,8793930000000,6),(19,19,'Building(s) on Level: 19','',15.9,26511400000,8793930000000,26467800000,8793920000000,6),(20,20,'Building(s) on Level: 20','',16.4,26467800000,8793920000000,26464900000,8793900000000,6),(21,21,'Building(s) on Level: 21','',17.1,26464900000,8793900000000,26439300000,8793870000000,6),(22,22,'Building(s) on Level: 22','',17.4,26439300000,8793870000000,26418500000,8793850000000,6),(23,23,'Building(s) on Level: 23','',17.8,26418500000,8793850000000,26390800000,8793820000000,6),(24,24,'Building(s) on Level: 24','',18.4,26390800000,8793820000000,26363000000,8793800000000,6),(25,25,'Building(s) on Level: 25','',19.1,26363000000,8793800000000,26339400000,8793770000000,6),(26,26,'Create 1000 Detail Views','',71.3,26339300000,8793770000000,26312900000,8793750000000,6),(27,27,'Create 50 Plans and Sheets','',103.6,26312900000,8793750000000,25962700000,8793510000000,6),(28,28,'Save Model to Temp Directory','',6,25962700000,8793510000000,25993700000,8793460000000,6),(29,29,'Open Model from Temp','',6.9,25993700000,8793460000000,25984700000,8793530000000,6),(30,30,'Export DWF Files','',111.4,25984700000,8793530000000,0,0,6),(31,31,'Open 3D View','',108.4,25984200000,8793520000000,0,0,6),(32,32,'Adjust Types','',106.6,25986000000,8793550000000,0,0,6),(33,1,'Building(s) on Level: 1','',8.1,26785900000,8794230000000,26752800000,8794220000000,7),(34,2,'Building(s) on Level: 2','',7.5,26752800000,8794220000000,26738000000,8794200000000,7),(35,3,'Building(s) on Level: 3','',8,26738000000,8794200000000,26726500000,8794200000000,7),(36,4,'Building(s) on Level: 4','',8.4,26726500000,8794200000000,26716600000,8794170000000,7),(37,5,'Building(s) on Level: 5','',8.9,26716600000,8794170000000,26701700000,8794170000000,7),(38,6,'Building(s) on Level: 6','',9.5,26701700000,8794170000000,26685100000,8794150000000,7),(39,7,'Building(s) on Level: 7','',9.8,26685100000,8794150000000,26673700000,8794130000000,7),(40,8,'Building(s) on Level: 8','',10.3,26673700000,8794130000000,26668500000,8794120000000,7),(41,9,'Building(s) on Level: 9','',10.8,26668500000,8794120000000,26655000000,8794100000000,7),(42,10,'Building(s) on Level: 10','',11.3,26655000000,8794100000000,26647100000,8794090000000,7),(43,11,'Building(s) on Level: 11','',11.9,26647100000,8794090000000,26639800000,8794090000000,7),(44,12,'Building(s) on Level: 12','',12.6,26639800000,8794090000000,26627300000,8794050000000,7),(45,13,'Building(s) on Level: 13','',12.8,26627300000,8794050000000,26610200000,8794040000000,7),(46,14,'Building(s) on Level: 14','',13.3,26610200000,8794040000000,26591500000,8794020000000,7),(47,15,'Building(s) on Level: 15','',13.8,26591500000,8794020000000,26570000000,8794020000000,7),(48,16,'Building(s) on Level: 16','',14.4,26570000000,8794020000000,26553100000,8793990000000,7),(49,17,'Building(s) on Level: 17','',15.1,26553100000,8793990000000,26532100000,8793970000000,7),(50,18,'Building(s) on Level: 18','',15.4,26532100000,8793970000000,26511400000,8793930000000,7),(51,19,'Building(s) on Level: 19','',15.9,26511400000,8793930000000,26467800000,8793920000000,7),(52,20,'Building(s) on Level: 20','',16.4,26467800000,8793920000000,26464900000,8793900000000,7),(53,21,'Building(s) on Level: 21','',17.1,26464900000,8793900000000,26439300000,8793870000000,7),(54,22,'Building(s) on Level: 22','',17.4,26439300000,8793870000000,26418500000,8793850000000,7),(55,23,'Building(s) on Level: 23','',17.8,26418500000,8793850000000,26390800000,8793820000000,7),(56,24,'Building(s) on Level: 24','',18.4,26390800000,8793820000000,26363000000,8793800000000,7),(57,25,'Building(s) on Level: 25','',19.1,26363000000,8793800000000,26339400000,8793770000000,7),(58,26,'Create 1000 Detail Views','',71.3,26339300000,8793770000000,26312900000,8793750000000,7),(59,27,'Create 50 Plans and Sheets','',103.6,26312900000,8793750000000,25962700000,8793510000000,7),(60,28,'Save Model to Temp Directory','',6,25962700000,8793510000000,25993700000,8793460000000,7),(61,29,'Open Model from Temp','',6.9,25993700000,8793460000000,25984700000,8793530000000,7),(62,30,'Export DWF Files','',111.4,25984700000,8793530000000,0,0,7),(63,31,'Open 3D View','',108.4,25984200000,8793520000000,0,0,7),(64,32,'Adjust Types','',106.6,25986000000,8793550000000,0,0,7),(65,1,'Building(s) on Level: 1','',8.1,26785900000,8794230000000,26752800000,8794220000000,8),(66,2,'Building(s) on Level: 2','',7.5,26752800000,8794220000000,26738000000,8794200000000,8),(67,3,'Building(s) on Level: 3','',8,26738000000,8794200000000,26726500000,8794200000000,8),(68,4,'Building(s) on Level: 4','',8.4,26726500000,8794200000000,26716600000,8794170000000,8),(69,5,'Building(s) on Level: 5','',8.9,26716600000,8794170000000,26701700000,8794170000000,8),(70,6,'Building(s) on Level: 6','',9.5,26701700000,8794170000000,26685100000,8794150000000,8),(71,7,'Building(s) on Level: 7','',9.8,26685100000,8794150000000,26673700000,8794130000000,8),(72,8,'Building(s) on Level: 8','',10.3,26673700000,8794130000000,26668500000,8794120000000,8),(73,9,'Building(s) on Level: 9','',10.8,26668500000,8794120000000,26655000000,8794100000000,8),(74,10,'Building(s) on Level: 10','',11.3,26655000000,8794100000000,26647100000,8794090000000,8),(75,11,'Building(s) on Level: 11','',11.9,26647100000,8794090000000,26639800000,8794090000000,8),(76,12,'Building(s) on Level: 12','',12.6,26639800000,8794090000000,26627300000,8794050000000,8),(77,13,'Building(s) on Level: 13','',12.8,26627300000,8794050000000,26610200000,8794040000000,8),(78,14,'Building(s) on Level: 14','',13.3,26610200000,8794040000000,26591500000,8794020000000,8),(79,15,'Building(s) on Level: 15','',13.8,26591500000,8794020000000,26570000000,8794020000000,8),(80,16,'Building(s) on Level: 16','',14.4,26570000000,8794020000000,26553100000,8793990000000,8),(81,17,'Building(s) on Level: 17','',15.1,26553100000,8793990000000,26532100000,8793970000000,8),(82,18,'Building(s) on Level: 18','',15.4,26532100000,8793970000000,26511400000,8793930000000,8),(83,19,'Building(s) on Level: 19','',15.9,26511400000,8793930000000,26467800000,8793920000000,8),(84,20,'Building(s) on Level: 20','',16.4,26467800000,8793920000000,26464900000,8793900000000,8),(85,21,'Building(s) on Level: 21','',17.1,26464900000,8793900000000,26439300000,8793870000000,8),(86,1,'Building(s) on Level: 1','',8.1,26785900000,8794230000000,26752800000,8794220000000,9),(87,2,'Building(s) on Level: 2','',7.5,26752800000,8794220000000,26738000000,8794200000000,9),(88,3,'Building(s) on Level: 3','',8,26738000000,8794200000000,26726500000,8794200000000,9),(89,4,'Building(s) on Level: 4','',8.4,26726500000,8794200000000,26716600000,8794170000000,9),(90,5,'Building(s) on Level: 5','',8.9,26716600000,8794170000000,26701700000,8794170000000,9),(91,6,'Building(s) on Level: 6','',9.5,26701700000,8794170000000,26685100000,8794150000000,9),(92,7,'Building(s) on Level: 7','',9.8,26685100000,8794150000000,26673700000,8794130000000,9),(93,8,'Building(s) on Level: 8','',10.3,26673700000,8794130000000,26668500000,8794120000000,9),(94,9,'Building(s) on Level: 9','',10.8,26668500000,8794120000000,26655000000,8794100000000,9),(95,10,'Building(s) on Level: 10','',11.3,26655000000,8794100000000,26647100000,8794090000000,9),(96,11,'Building(s) on Level: 11','',11.9,26647100000,8794090000000,26639800000,8794090000000,9),(97,12,'Building(s) on Level: 12','',12.6,26639800000,8794090000000,26627300000,8794050000000,9),(98,13,'Building(s) on Level: 13','',12.8,26627300000,8794050000000,26610200000,8794040000000,9),(99,14,'Building(s) on Level: 14','',13.3,26610200000,8794040000000,26591500000,8794020000000,9),(100,15,'Building(s) on Level: 15','',13.8,26591500000,8794020000000,26570000000,8794020000000,9),(101,16,'Building(s) on Level: 16','',14.4,26570000000,8794020000000,26553100000,8793990000000,9),(102,17,'Building(s) on Level: 17','',15.1,26553100000,8793990000000,26532100000,8793970000000,9),(103,18,'Building(s) on Level: 18','',15.4,26532100000,8793970000000,26511400000,8793930000000,9),(104,19,'Building(s) on Level: 19','',15.9,26511400000,8793930000000,26467800000,8793920000000,9),(105,20,'Building(s) on Level: 20','',16.4,26467800000,8793920000000,26464900000,8793900000000,9),(106,21,'Building(s) on Level: 21','',17.1,26464900000,8793900000000,26439300000,8793870000000,9),(107,22,'Building(s) on Level: 22','',17.4,26439300000,8793870000000,26418500000,8793850000000,9),(108,23,'Building(s) on Level: 23','',17.8,26418500000,8793850000000,26390800000,8793820000000,9),(109,24,'Building(s) on Level: 24','',18.4,26390800000,8793820000000,26363000000,8793800000000,9),(110,25,'Building(s) on Level: 25','',19.1,26363000000,8793800000000,26339400000,8793770000000,9),(111,26,'Create 1000 Detail Views','',71.3,26339300000,8793770000000,26312900000,8793750000000,9),(112,27,'Create 50 Plans and Sheets','',103.6,26312900000,8793750000000,25962700000,8793510000000,9),(113,28,'Save Model to Temp Directory','',6,25962700000,8793510000000,25993700000,8793460000000,9),(114,29,'Open Model from Temp','',6.9,25993700000,8793460000000,25984700000,8793530000000,9),(115,30,'Export DWF Files','',111.4,25984700000,8793530000000,0,0,9),(116,31,'Open 3D View','',108.4,25984200000,8793520000000,0,0,9),(117,32,'Adjust Types','',106.6,25986000000,8793550000000,0,0,9);
/*!40000 ALTER TABLE `timer_result` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-25 12:47:17
