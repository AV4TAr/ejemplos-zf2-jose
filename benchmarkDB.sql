SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE SCHEMA IF NOT EXISTS `creatio_casetest` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `creatio_casetest` ;

-- -----------------------------------------------------
-- Table `creatio_casetest`.`session_data`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`session_data` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(45) NULL,
  `addinVer` VARCHAR(45) NULL,
  `revitBuild` VARCHAR(45) NULL,
  `revitName` VARCHAR(45) NULL,
  `revitLanguage` VARCHAR(45) NULL,
  `session_id` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `creatio_casetest`.`timer_result`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`timer_result` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sequence` INT(25) NULL,
  `name` VARCHAR(45) NULL,
  `comment` TEXT NULL,
  `seconds` FLOAT NULL,
  `availPhysMem_start` FLOAT NULL,
  `availVirtMem_start` FLOAT NULL,
  `availPhysMem_end` FLOAT NULL,
  `availVirtMem_end` FLOAT NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_timer_result_session_data_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_timer_result_session_data`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `creatio_casetest`.`hw_spec_general`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`hw_spec_general` (
  `id` INT NULL AUTO_INCREMENT,
  `os_name` VARCHAR(45) NULL,
  `os_version` VARCHAR(45) NULL,
  `os_bits` INT(25) NULL,
  `computer_name` VARCHAR(45) NULL,
  `available_physical_memory` FLOAT NULL,
  `available_virtual_memory` FLOAT NULL,
  `total_pysical_memory` FLOAT NULL,
  `total_virtual_memory` FLOAT NULL,
  `number_processors` INT NULL,
  `number_logical_processors` INT NULL,
  `pc_system_type` INT(25) NULL,
  `win_user_name` VARCHAR(45) NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hw_spec_session_data1_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_hw_spec_session_data1`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '	';


-- -----------------------------------------------------
-- Table `creatio_casetest`.`hw_spec_video`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`hw_spec_video` (
  `id` INT NULL AUTO_INCREMENT,
  `accelerator_capabilities` VARCHAR(45) NULL,
  `adapter_compatibility` VARCHAR(45) NULL,
  `adapter_dac_type` VARCHAR(45) NULL,
  `adapter_ram` INT NULL,
  `availability` INT NULL,
  `capability_descriptions` TEXT NULL,
  `caption` VARCHAR(45) NULL,
  `color_table_entries` INT(25) NULL,
  `config_manager_error_code` INT(25) NULL,
  `config_manager_user_config` VARCHAR(10) NULL,
  `creation_class_name` VARCHAR(45) NULL,
  `current_bits_per_pixel` INT(25) NULL,
  `current_horizontal_resolution` INT(25) NULL,
  `current_number_colors` FLOAT NULL,
  `current_number_columns` INT(25) NULL,
  `current_number_rows` INT(25) NULL,
  `current_refresh_rate` INT(25) NULL,
  `current_scan_mode` INT(25) NULL,
  `current_vertical_resolution` INT(25) NULL,
  `description` VARCHAR(45) NULL,
  `device_id` VARCHAR(45) NULL,
  `device_specific_pens` INT(25) NULL,
  `dither_type` INT(25) NULL,
  `driver_date` DATE NULL,
  `driver_version` VARCHAR(45) NULL,
  `error_cleared` TINYINT(1) NULL,
  `error_description` VARCHAR(45) NULL,
  `icm_intent` INT(25) NULL,
  `icm_method` INT(25) NULL,
  `inf_file_name` VARCHAR(45) NULL,
  `inf_section` VARCHAR(45) NULL,
  `install_date` DATE NULL,
  `installed_display_drivers` TEXT NULL,
  `last_error_code` INT(25) NULL,
  `max_memory_supported` INT(25) NULL,
  `max_number_controlled` INT(25) NULL,
  `max_refresh_rate` INT(25) NULL,
  `min_refresh_rate` INT(25) NULL,
  `monochrome` TINYINT(1) NULL,
  `name` VARCHAR(45) NULL,
  `number_color_planes` INT(25) NULL,
  `number_video_pages` INT(25) NULL,
  `pnp_device_id` VARCHAR(90) NULL,
  `power_management_capabilities` INT(25) NULL,
  `power_management_supported` TINYINT(1) NULL,
  `protocol_supported` INT(25) NULL,
  `reserved_system_palette_entries` INT(25) NULL,
  `specification_version` INT(25) NULL,
  `status` VARCHAR(20) NULL,
  `status_info` VARCHAR(45) NULL,
  `system_palette_entries` INT(25) NULL,
  `time_last_reset` DATE NULL,
  `video_architecture` INT(25) NULL,
  `video_memory_type` INT(25) NULL,
  `video_mode` INT(25) NULL,
  `video_mode_description` TEXT NULL,
  `video_processor` VARCHAR(45) NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hw_spec_video_session_data1_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_hw_spec_video_session_data1`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `creatio_casetest`.`hw_spec_processor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`hw_spec_processor` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `manufacturer` VARCHAR(45) NULL,
  `current_clock_speed` INT(25) NULL,
  `load_percentage` INT(25) NULL,
  `l2_cache_speed` INT(25) NULL,
  `l3_cache_speed` INT(25) NULL,
  `l2_cache` INT(25) NULL,
  `l3_cache` INT(25) NULL,
  `max_clock_speed` INT(25) NULL,
  `number_cores` INT NULL,
  `number_logical_processors` INT NULL,
  `current_voltage` INT(25) NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hw_spec_processor_session_data1_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_hw_spec_processor_session_data1`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `creatio_casetest`.`hw_spec_ram`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`hw_spec_ram` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `manufacturer` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `capacity` FLOAT NULL,
  `data_width` INT(25) NULL,
  `form_factor` INT(25) NULL,
  `interleave_data_depth` INT(25) NULL,
  `interleave_position` INT(25) NULL,
  `memory_type` INT(25) NULL,
  `speed` INT(25) NULL,
  `total_width` INT(25) NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hw_spec_ram_session_data1_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_hw_spec_ram_session_data1`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `creatio_casetest`.`hw_spec_hd`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`hw_spec_hd` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `model` VARCHAR(45) NULL,
  `manufacturer` VARCHAR(45) NULL,
  `description` VARCHAR(45) NULL,
  `size` FLOAT NULL,
  `serial_number` VARCHAR(45) NULL,
  `interface_type` VARCHAR(45) NULL,
  `bytes_per_sector` INT(25) NULL,
  `compression_method` VARCHAR(45) NULL,
  `media_type` VARCHAR(45) NULL,
  `partitions` INT NULL,
  `status` VARCHAR(45) NULL,
  `total_cylinders` INT NULL,
  `total_heads` INT NULL,
  `total_sectors` INT NULL,
  `total_tracks` INT NULL,
  `tracks_per_cylinder` INT NULL,
  `default_block_size` INT(25) NULL,
  `firmware_revision` VARCHAR(45) NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hw_spec_hd_session_data1_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_hw_spec_hd_session_data1`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `creatio_casetest`.`hw_spec_drive_partition`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`hw_spec_drive_partition` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `available_free_space` FLOAT NULL,
  `total_size` FLOAT NULL,
  `drive_format` VARCHAR(45) NULL,
  `session_data_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hw_spec_drive_partition_session_data1_idx` (`session_data_id` ASC),
  CONSTRAINT `fk_hw_spec_drive_partition_session_data1`
    FOREIGN KEY (`session_data_id`)
    REFERENCES `creatio_casetest`.`session_data` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

USE `creatio_casetest` ;

-- -----------------------------------------------------
-- Placeholder table for view `creatio_casetest`.`view1`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `creatio_casetest`.`view1` (`id` INT);

-- -----------------------------------------------------
--  routine1
-- -----------------------------------------------------

DELIMITER $$
USE `creatio_casetest`$$
CREATE PROCEDURE `routine1` ()
BEGIN

END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `creatio_casetest`.`view1`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `creatio_casetest`.`view1`;
USE `creatio_casetest`;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
