<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>BIM Benchmarking Tool</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="../../css/base.css">
	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel="stylesheet" href="../../css/layout.css">
	<!-- BARS //-->
	<!-- <link href="../css/demo.css" rel="stylesheet" type="text/css" /> //-->
	<link rel="stylesheet" href="../../css/jqbar.css" />


	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<!--
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	-->
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script src="../../js/themes/gray.js"></script>

	<script type="text/javascript">

	$(function () {
	    // set the theme
	    Highcharts.setOptions({
			
	        colors: ['#33ff99', '#33ffff', '#99ff33', '#cc33ff', '#ffff99', '#ff0066', '#6699cc', '#FFF263', '#6AF9C4'],
	        chart: {
	        	height: 170,
	        	width: 800,
	            backgroundColor: {
	                linearGradient: [0, 0, 500, 500],
	                stops: [
	                    [0, 'rgb(51, 51, 51)'],
	                    [1, 'rgb(51, 51, 51)']
	                ]
	            },
	            borderWidth: 0,
	            plotBackgroundColor: 'rgba(51, 51, 51, .9)',
	            plotShadow: true,
	            plotBorderWidth: 1,
	            borderRadius: 0,
	            lineColor: '#515151'
	        },
	        title: {
	            style: { 
	                color: '#f2f2f2',
	                font: 'bold 12px "HelveticaNeue", "Helvetica Neue", sans-serif'
	            }
	        },
	        subtitle: {
	            style: { 
	                color: '#c1c1c1',
	                font: 'bold 12px "HelveticaNeue", "Helvetica Neue", sans-serif'
	            }
	        },
	        xAxis: {
	            gridLineWidth: 1,
	            lineColor: '#c1c1c1',
	            tickColor: '#f2f2f2',
	            labels: {
	                style: {
	                    color: '#c1c1c1',
	                    font: '12px "HelveticaNeue", "Helvetica Neue", sans-serif'
	                }
	            },
	            title: {
	                style: {
	                    color: '#c1c1c1',
	                    fontWeight: 'bold',
	                    fontSize: '12px',
	                    fontFamily: '"HelveticaNeue", "Helvetica Neue", sans-serif'
	    
	                }                
	            }
	        },
	        yAxis: {
	        	gridLineColor: '#555555',
	        	minorGridLineColor: '#5a5a5a',
	            reversed: true,
	            alternateGridColor: null,
	            minorTickInterval: 'auto',
	            lineColor: '#c1c1c1',
	            lineWidth: 1,
	            tickWidth: 1,
	            tickColor: '#000',
	            labels: {
	                style: {
	                    color: '#c1c1c1',
	                    font: '11px "HelveticaNeue", "Helvetica Neue", sans-serif'
	                }
	            },
	            title: {
	                style: {
	                    color: '#c1c1c1',
	                    fontWeight: 'normal',
	                    fontSize: '12px',
	                    fontFamily: '"HelveticaNeue", "Helvetica Neue", sans-serif'
	                }                
	            }
	        },
	        legend: {
	        	itemWidth: 105,
	        	borderRadius: 0,
	            itemStyle: {            
	                font: '7pt "HelveticaNeue", "Helvetica Neue", sans-serif',
	                color: '#c1c1c1'
	    
	            },
	            itemHoverStyle: {
	                color: '#f2f2f2'
	            },
	            itemHiddenStyle: {
	                color: 'gray'
	            }
	        },
	        credits: {
		        enabled: false,
	            style: {
	                right: '10px'
	            }
	        },
	        labels: {
	            style: {
	                color: '#606060'
	            }
	        },
        	tooltip: {
            	borderRadius: 0,
            	backgroundColor: 'rgba(105, 105, 105, 0.85)',
                font: '8pt "HelveticaNeue", "Helvetica Neue", sans-serif',
                style: {
                    padding: 4,
                    fontWeight: 'bold',
                    color: '#f1f1f1'
                		}
           			 }
	    });
	    
	    // create the chart
	    $('#chart').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Tests'
            },
            xAxis: {
                categories: ['.']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Time in seconds'
                }
            },
            legend: {
                backgroundColor: '#333333',
                reversed: false
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
                series: [{
                name: 'Buildings on Levels',
                data: [<?php echo $timerPartial;?>]
            }, {
                name: 'Plans & Sheets',
                data: [<?php echo $timerArray[27];?>]
            }, {
                name: 'Detail Views',
                data: [<?php echo $timerArray[26];?>]
            }, {
                name: 'Open / Save',
                data: [<?php echo $timerArray[28];?>]
            }, {
                name: 'Export DWF',
                data: [<?php echo $timerArray[30];?>]
            }, {
                name: 'Open 3D View',
                data: [<?php echo $timerArray[31];?>]
            }, {
                name: 'Adjust Types',
                data: [<?php echo $timerArray[32];?>]
            }]
        });
	});
$(function () {
       
    });
    
		</script>

</head>
<body>

			<!-- Primary Page Layout
			================================================== -->

	<div class="container">
		<div class="sixteen columns">
			BIM <span class="grey">BENCHMARK</span><span class="grey" style="float:right; font-size: 0.75em;">SETTINGS &nbsp;&nbsp;&nbsp;&nbsp;<a href="../userList">LOGOUT</a></span>
			<hr />
		</div>
		<div class="sixteen columns">
			<span style="font-size: 45px; font-weight: bold;">MY RESULTS</span><span style="float:right; color: #5aadd9; font-size: 0.75em;">SHARE</span>
			<br /><br />
		</div>
		<?php 
		include 'viewMachines.php';		
		include 'viewTests.php';
		include 'viewTestResults.php';
		include 'viewFooter.php';
		?>
