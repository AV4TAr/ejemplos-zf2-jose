<?php
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';
require_once 'vendor/autoload.php';

use Zend\Session\Container;

class UserSession
{
	private $user_session;
	public function __construct(Container $contenedor)
	{
		$this->user_session = $contenedor;
	}

	public function save($key, $value)
	{
		$this->user_session->$key = $value;
	}

	public function get($key)
	{
		return $this->user_session->$key;
	}
}