		<!--  START TEST RESULT DATA -->	
		<div class="four columns">
			<span class="result block title">Total Score</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerManager->getTimersByTest($session_data_id));?></span><br/>
			<span class="result block caption">minutes</span><br /><br />
		</div>
		<div class="twelve columns">
		<div id="chart" style="min-width: 300px; height: 180px; margin: 0 auto; margin-left: -90px; margin-top: -30px;"></div>
		</div>

		<div class="four columns" style="margin-left: 9px; border-right: 1px solid grey">
			<span class="result block title">Buildings on Level</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerPartial);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
		<div class="eight columns">
			<div style="margin-top: -30px; margin-left: -5px;">
					<div id="bar-1">
					</div>
					<div id="bar-2">
					</div>
					<div id="bar-3">
					</div>
					<div id="bar-4">
					</div>
					<div id="bar-5">
					</div>
					<div id="bar-6">
					</div>
					<div id="bar-7">
					</div>
					<div id="bar-8">
					</div>
					<div id="bar-9">
					</div>
					<div id="bar-10">
					</div>
					<div id="bar-11">
					</div>
					<div id="bar-12">
					</div>
					<div id="bar-13">
					</div>
					<div id="bar-14">
					</div>
					<div id="bar-15">
					</div>
					<div id="bar-16">
					</div>
					<div id="bar-17">
					</div>
					<div id="bar-18">
					</div>
					<div id="bar-19">
					</div>
					<div id="bar-20">
					</div>
					<div id="bar-21">
					</div>
					<div id="bar-22">
					</div>
					<div id="bar-23">
					</div>
					<div id="bar-24">
					</div>
					<div id="bar-25">
					</div>
				</div>

			</div>
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Plans & Sheets</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[27]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number cyan"><?php echo $timerManager->estimateNumber("Plans&Sheets",$timerArray[27]);?></span><br />
			<span class="result block caption">Estimated # of Plans & Sheets created in 10min</span><br />
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Detail Views</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[26]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number yellow-green"><?php echo $timerManager->estimateNumber("DetailViews",$timerArray[26]);?></span><br />
			<span class="result block caption">Estimated # of Detail Views created in 10min</span><br />
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Open / Save</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[28]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number magenta"><?php echo $timerManager->estimateNumber("OpenSave",$timerArray[28]);?></span><br />
			<span class="result block caption">Estimated # of Open/Save operations performed in 10min </span><br />
		</div>

		<div class="twelve columns"><br /><br />
			<hr />
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Export DWF</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[30]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number yellow"><?php echo $timerManager->estimateNumber("ExportDWF",$timerArray[30]);?></span><br />
			<span class="result block caption">Estimated # of 3D DWF files exported in 10min</span><br />
		</div>
		<div class="four columns" style="height: 400px; border: 0px solid white;">
			<span class="result block title">Open 3D View</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[31]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number red" style=" padding-top:50px; "><?php echo $timerManager->estimateNumber("3DView",$timerArray[31]);?></span><br />
			<span class="result block caption">Estimated # of 3D Views opened in 10 min</span><br />
		</div>
		<div class="four columns" style="height: 400px; border: 0px solid white;">
			<span class="result block title">Adjust Types</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[32]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number blue" style=" padding-top:50px; "><?php echo $timerManager->estimateNumber("AdjustTypes",$timerArray[32]);?></span><br />
			<span class="result block caption">Estimated # of Types adjusted in 10min</span><br />
		</div>
	</div><!-- container -->
	<script src="../../js/jquery.min.js" type="text/javascript"></script>
	<script src="../../js/jqbar.js" type="text/javascript"></script>
    <script type="text/javascript">

    $(document).ready(function () {

    $('#bar-1').jqbar({ label: '<?php echo $timerArray[1]; ?>', value: <?php echo $timerArray[1]; ?>, barColor: '#33ff99' });
    	
    $('#bar-2').jqbar({ label: '<?php echo $timerArray[2]; ?>', value: <?php echo $timerArray[2]; ?>, barColor: '#33ff99' });

	$('#bar-3').jqbar({ label: '<?php echo $timerArray[3]; ?>', value: <?php echo $timerArray[3]; ?>, barColor: '#33ff99' });

	$('#bar-4').jqbar({ label: '<?php echo $timerArray[4]; ?>', value: <?php echo $timerArray[4]; ?>, barColor: '#33ff99' });

	$('#bar-5').jqbar({ label: '<?php echo $timerArray[5]; ?>', value: <?php echo $timerArray[5]; ?>, barColor: '#33ff99' });

	$('#bar-6').jqbar({ label: '<?php echo $timerArray[6]; ?>', value: <?php echo $timerArray[6]; ?>, barColor: '#33ff99' });

	$('#bar-7').jqbar({ label: '<?php echo $timerArray[7]; ?>', value: <?php echo $timerArray[7]; ?>, barColor: '#33ff99' });

	$('#bar-8').jqbar({ label: '<?php echo $timerArray[8]; ?>', value: <?php echo $timerArray[8]; ?>, barColor: '#33ff99' });

	$('#bar-9').jqbar({ label: '<?php echo $timerArray[9]; ?>', value: <?php echo $timerArray[9]; ?>, barColor: '#33ff99' });

	$('#bar-10').jqbar({ label: '<?php echo $timerArray[10]; ?>', value: <?php echo $timerArray[10]; ?>, barColor: '#33ff99' });

	$('#bar-11').jqbar({ label: '<?php echo $timerArray[11]; ?>', value: <?php echo $timerArray[11]; ?>, barColor: '#33ff99' });

	$('#bar-12').jqbar({ label: '<?php echo $timerArray[12]; ?>', value: <?php echo $timerArray[12]; ?>, barColor: '#33ff99' });
		
	$('#bar-13').jqbar({ label: '<?php echo $timerArray[13]; ?>', value: <?php echo $timerArray[13]; ?>, barColor: '#33ff99' });

	$('#bar-14').jqbar({ label: '<?php echo $timerArray[14]; ?>', value: <?php echo $timerArray[14]; ?>, barColor: '#33ff99' });

	$('#bar-15').jqbar({ label: '<?php echo $timerArray[15]; ?>', value: <?php echo $timerArray[15]; ?>, barColor: '#33ff99' });

	$('#bar-16').jqbar({ label: '<?php echo $timerArray[16]; ?>', value: <?php echo $timerArray[16]; ?>, barColor: '#33ff99' });

	$('#bar-17').jqbar({ label: '<?php echo $timerArray[17]; ?>', value: <?php echo $timerArray[17]; ?>, barColor: '#33ff99' });

	$('#bar-18').jqbar({ label: '<?php echo $timerArray[18]; ?>', value: <?php echo $timerArray[18]; ?>, barColor: '#33ff99' });

	$('#bar-19').jqbar({ label: '<?php echo $timerArray[19]; ?>', value: <?php echo $timerArray[19]; ?>, barColor: '#33ff99' });

	$('#bar-20').jqbar({ label: '<?php echo $timerArray[20]; ?>', value: <?php echo $timerArray[20]; ?>, barColor: '#33ff99' });

	$('#bar-21').jqbar({ label: '<?php echo $timerArray[21]; ?>', value: <?php echo $timerArray[21]; ?>, barColor: '#33ff99' });

	$('#bar-22').jqbar({ label: '<?php echo $timerArray[22]; ?>', value: <?php echo $timerArray[22]; ?>, barColor: '#33ff99' });

	$('#bar-23').jqbar({ label: '<?php echo $timerArray[23]; ?>', value: <?php echo $timerArray[23]; ?>, barColor: '#33ff99' });

	$('#bar-24').jqbar({ label: '<?php echo $timerArray[24]; ?>', value: <?php echo $timerArray[24]; ?>, barColor: '#33ff99' });

	$('#bar-25').jqbar({ label: '<?php echo $timerArray[25]; ?>', value: <?php echo $timerArray[25]; ?>, barColor: '#33ff99' });

	});
	</script>
	<script src="../../js/highcharts.js"></script>
	
	<!-- //-->
	<!-- End Document
	================================================== -->	