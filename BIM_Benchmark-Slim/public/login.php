<?php
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';
require_once 'vendor/autoload.php';

use Zend\Session\Container;
require_once 'controlSession.php';

$email = $user;
// check @ login -> session start

$UserSession = new UserSession(new Container('userSession'));

$UserSession->save("email", $email);

echo "User selected: <a href='../results/'>".$email."</a><br /></li>";