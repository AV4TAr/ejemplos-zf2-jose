<?php

chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

//composer
require 'vendor/autoload.php';

set_time_limit(0);

use Zend\Db\Sql;

class TestManager
{
	private $db_config;
	private $adapter;
	
	public function __construct($db_config)
	{
		$this->db_config=$db_config;
		$this->adapter=new Zend\Db\Adapter\Adapter($this->db_config);
	}

	public function listTests_BM($session_email)
	{
	
		$sql = new Zend\Db\Sql\Sql($this->adapter);
	
		$select = $sql->select();

		$select->from(array('session_data' => 'session_data'))  // base table
		->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
				'session_data.id = hw_spec_general.session_data_id')// join expression
				->group('hw_spec_general.computer_name')
				->where->like('session_data.email' , $session_email);
			
		$selectString = $sql->getSqlStringForSqlObject($select);
		$theadapter = $this->adapter;
		$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);
	
		return $results;	
	}
	
	public function listTestsBU_BM($session_email, $computer_name)
	{
		$sql = new Zend\Db\Sql\Sql($this->adapter);
				
			$select = $sql->select();
			$select->from(array('session_data' => 'session_data'))  // base table
			->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
					'session_data.id = hw_spec_general.session_data_id')// join expression

			->where(array('hw_spec_general.computer_name' => $computer_name, 'session_data.email' => $session_email));

			$selectString = $sql->getSqlStringForSqlObject($select);
			$theadapter = $this->adapter;
			$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);
			
			return $results;
	}
		
	public function getMachineData($session_email, $computer_name)
	{	
		$sql = new Zend\Db\Sql\Sql($this->adapter);
		
		$select = $sql->select();
		$select->from(array('session_data' => 'session_data'))  // base table
		->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
				'session_data.id = hw_spec_general.session_data_id', array('os_name', 'os_version'))// join expression
		->join(array('hw_spec_processor' => 'hw_spec_processor'),     // join table with alias
				'session_data.id = hw_spec_processor.session_data_id', array('name'))// join expression
		->join(array('hw_spec_video' => 'hw_spec_video'),     // join table with alias
				'session_data.id = hw_spec_video.session_data_id', array('caption'))// join expression				
		->where(array('hw_spec_general.computer_name' => $computer_name, 'session_data.email' => $session_email));

		
		$selectString = $sql->getSqlStringForSqlObject($select);
		$theadapter = $this->adapter;
		$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);
		
		return $results; 

	}
	
}

class TimerManager
{
	private $db_config;
	private $adapter;

	public function __construct($db_config)
	{
		$this->db_config=$db_config;
		$this->adapter=new Zend\Db\Adapter\Adapter($this->db_config);
	}

	public function getTimersByTest($session_data_id)
	{
		$projectTable = new Zend\Db\TableGateway\TableGateway('timer_result',$this->adapter);
		$rowset = $projectTable->select(array('session_data_id' => $session_data_id));
		$timerTotal = 0;
		foreach ($rowset as $projectRow) {
			$timerTotal = $timerTotal + $projectRow['seconds'];
		}
		return $timerTotal;
	}
	
	public function getTimers($theTable, $session_data_id,&$timer)
	{
		$projectTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$rowset = $projectTable->select(array('session_data_id' => $session_data_id));
		$jqbar_data = array();
		$i = 1;
		foreach ($rowset as $projectRow) {
			$timer[$i] = $projectRow['seconds'];
			$i++;
		}
	}
	
	public function toMins($theSecs)
	{
		if($theSecs < 60){
				
			if($theSecs <= 9){
				return "0:0".ceil($theSecs);
			}else{
				return "0:".ceil($theSecs);
			}
		}else{
			$theMins = $theSecs / 60;
			$mod = ceil(fmod($theSecs, 60));
			sprintf("%1.2f",$mod);
			if ($mod <= 9){
				return floor($theMins).":0".$mod;
			}else{
				return floor($theMins).":".$mod;
			}		
		}
	}
	
	public function estimateNumber($type, $seconds)
	{
		switch ($type){
			case "Plans&Sheets":
				$number = 50;
				break;
			case "DetailViews":
				$number = 1000;
				break;
			case "OpenSave":
				$number = 1;
				break;
			case "ExportDWF":
				$number = 1;
				break;
			case "3DView":
				$number = 1;
				break;
			case "AdjustTypes":
				$number = 10;
				break;
		}
		$estimate = ceil(($number*600)/$seconds);
		return $estimate;
	}
}
