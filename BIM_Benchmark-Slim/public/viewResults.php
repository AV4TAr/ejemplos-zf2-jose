<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>BIM Benchmarking Tool</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Mobile Specific Metas
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="../../css/base.css">
	<link rel="stylesheet" href="../../css/skeleton.css">
	<link rel="stylesheet" href="../../css/layout.css">
	<!-- BARS //-->
	<!-- <link href="css/demo.css" rel="stylesheet" type="text/css" /> //-->
	<link rel="stylesheet" href="../../css/jqbar.css" />


	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<!--
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	-->
	<script src="http://cdn.jquerytools.org/1.2.7/full/jquery.tools.min.js"></script>
	<script src="../../js/themes/gray.js"></script>

	<script type="text/javascript">

	$(function () {
	    // set the theme
	    Highcharts.setOptions({
			
	        colors: ['#33ff99', '#33ffff', '#99ff33', '#cc33ff', '#ffff99', '#ff0066', '#6699cc', '#FFF263', '#6AF9C4'],
	        chart: {
	        	height: 170,
	        	width: 800,
	            backgroundColor: {
	                linearGradient: [0, 0, 500, 500],
	                stops: [
	                    [0, 'rgb(51, 51, 51)'],
	                    [1, 'rgb(51, 51, 51)']
	                ]
	            },
	            borderWidth: 0,
	            plotBackgroundColor: 'rgba(51, 51, 51, .9)',
	            plotShadow: true,
	            plotBorderWidth: 1,
	            borderRadius: 0,
	            lineColor: '#515151'
	        },
	        title: {
	            style: { 
	                color: '#f2f2f2',
	                font: 'bold 12px "HelveticaNeue", "Helvetica Neue", sans-serif'
	            }
	        },
	        subtitle: {
	            style: { 
	                color: '#c1c1c1',
	                font: 'bold 12px "HelveticaNeue", "Helvetica Neue", sans-serif'
	            }
	        },
	        xAxis: {
	            gridLineWidth: 1,
	            lineColor: '#c1c1c1',
	            tickColor: '#f2f2f2',
	            labels: {
	                style: {
	                    color: '#c1c1c1',
	                    font: '12px "HelveticaNeue", "Helvetica Neue", sans-serif'
	                }
	            },
	            title: {
	                style: {
	                    color: '#c1c1c1',
	                    fontWeight: 'bold',
	                    fontSize: '12px',
	                    fontFamily: '"HelveticaNeue", "Helvetica Neue", sans-serif'
	    
	                }                
	            }
	        },
	        yAxis: {
	        	gridLineColor: '#555555',
	        	minorGridLineColor: '#5a5a5a',
	            reversed: true,
	            alternateGridColor: null,
	            minorTickInterval: 'auto',
	            lineColor: '#c1c1c1',
	            lineWidth: 1,
	            tickWidth: 1,
	            tickColor: '#000',
	            labels: {
	                style: {
	                    color: '#c1c1c1',
	                    font: '11px "HelveticaNeue", "Helvetica Neue", sans-serif'
	                }
	            },
	            title: {
	                style: {
	                    color: '#c1c1c1',
	                    fontWeight: 'normal',
	                    fontSize: '12px',
	                    fontFamily: '"HelveticaNeue", "Helvetica Neue", sans-serif'
	                }                
	            }
	        },
	        legend: {
	        	itemWidth: 105,
	        	borderRadius: 0,
	            itemStyle: {            
	                font: '7pt "HelveticaNeue", "Helvetica Neue", sans-serif',
	                color: '#c1c1c1'
	    
	            },
	            itemHoverStyle: {
	                color: '#f2f2f2'
	            },
	            itemHiddenStyle: {
	                color: 'gray'
	            }
	        },
	        credits: {
		        enabled: false,
	            style: {
	                right: '10px'
	            }
	        },
	        labels: {
	            style: {
	                color: '#606060'
	            }
	        },
        	tooltip: {
            	borderRadius: 0,
            	backgroundColor: 'rgba(105, 105, 105, 0.85)',
                font: '8pt "HelveticaNeue", "Helvetica Neue", sans-serif',
                style: {
                    padding: 4,
                    fontWeight: 'bold',
                    color: '#f1f1f1'
                		}
           			 }
	    });
	    
	    // create the chart
	    $('#chart').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Tests'
            },
            xAxis: {
                categories: ['.']
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Time in seconds'
                }
            },
            legend: {
                backgroundColor: '#333333',
                reversed: false
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
                series: [{
                name: 'Buildings on Levels',
                data: [<?php echo $timerPartial;?>]
            }, {
                name: 'Plans & Sheets',
                data: [<?php echo $timerArray[27];?>]
            }, {
                name: 'Detail Views',
                data: [<?php echo $timerArray[26];?>]
            }, {
                name: 'Open / Save',
                data: [<?php echo $timerArray[28];?>]
            }, {
                name: 'Export DWF',
                data: [<?php echo $timerArray[30];?>]
            }, {
                name: 'Open 3D View',
                data: [<?php echo $timerArray[31];?>]
            }, {
                name: 'Adjust Types',
                data: [<?php echo $timerArray[32];?>]
            }]
        });
	});
$(function () {
       
    });
    
		</script>

</head>
<body>

			<!-- Primary Page Layout
			================================================== -->

	<div class="container">
		<div class="sixteen columns">
			BIM <span class="grey">BENCHMARK</span><span class="grey" style="float:right; font-size: 0.75em;">SETTINGS &nbsp;&nbsp;&nbsp;&nbsp;LOGOUT</span>
			<hr />
		</div>
		<div class="sixteen columns">
			<span style="font-size: 45px; font-weight: bold;">MY RESULTS</span><span style="float:right; color: #5aadd9; font-size: 0.75em;">SHARE</span>
			<br /><br />
		</div>
		<div class="sixteen columns">
		<?php  	echo "<ul id='rec' name='cn'>";
				//$listTests_array = $testManager->listTests_BM($session_email);
				foreach ($listTests_array as $row) {
					echo "<li><a href='".$_SERVER['PHP_SELF']."?cn=".$row['computer_name']."&u=".$row['email']."'><span class='grey'>".$row['computer_name']."</a></li>";
				}
				echo "</ul>";
			?>				
		</div>		
		<?php 
			if(isset($_GET['cn'])){
			$computer_name = $_GET['cn'];;
			//$machineData = $testManager->getMachineData($session_email, $computer_name);
			foreach ($machineData as $row) {
				echo "Computer Name: ".$computer_name."<br />Processor: ".$row['name']."<br />Video: ".$row['caption']."<br />OS: ".$row['os_name']." / ".$row['os_version']."--";
			}
			
		?>
		<div class="sixteen columns">
			Recent Tests
		</div>
		<div class="sixteen columns">
			<!-- <div style="border: 2px solid white;height: 54px; margin-bottom: 15px;"> test 1 </div>//-->
			<ul id="rec" class="----recent-tests">
				<?php  			
				//$listTestsByUserByMachine = $testManager->listTestsBU_BM($session_email,$computer_name); 
				foreach ($listTestsByUserByMachine as $row) {
					$thisTimerSecs = $timerManager->getTimersByTest($row['session_data_id']);
					$thisTimer = $timerManager->toMins($thisTimerSecs);
					echo "<li><a href='".$_SERVER['PHP_SELF']."?cn=".$row['computer_name']."&u=".$row['email']."&id=".$row['session_data_id']."'><span class='grey'>".$row['session_data_id']."</span><br /><br /><span class='time'><span style='float: left; text-align: left;'>Time</span><span style='float: right; text-align: right;'>".$thisTimer."<span></span></a></li>";					
				}
				?>
			</ul>
		</div>
			<hr/>
		<?php if(isset($_GET['id'])){
			
			$session_data_id = $_GET['id'];

		?>
		<!--  START TEST RESULT DATA -->	
		<div class="four columns">
			<span class="result block title">Total Score</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerManager->getTimersByTest($session_data_id));?></span><br/>
			<span class="result block caption">minutes</span><br /><br />
		</div>
		<div class="twelve columns">
		<div id="chart" style="min-width: 300px; height: 180px; margin: 0 auto; margin-left: -90px; margin-top: -30px;"></div>
		</div>

		<div class="four columns" style="margin-left: 9px; border-right: 1px solid grey">
			<span class="result block title">Buildings on Level</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerPartial);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
		<div class="eight columns">
			<div style="margin-top: -30px; margin-left: -5px;">
					<div id="bar-1">
					</div>
					<div id="bar-2">
					</div>
					<div id="bar-3">
					</div>
					<div id="bar-4">
					</div>
					<div id="bar-5">
					</div>
					<div id="bar-6">
					</div>
					<div id="bar-7">
					</div>
					<div id="bar-8">
					</div>
					<div id="bar-9">
					</div>
					<div id="bar-10">
					</div>
					<div id="bar-11">
					</div>
					<div id="bar-12">
					</div>
					<div id="bar-13">
					</div>
					<div id="bar-14">
					</div>
					<div id="bar-15">
					</div>
					<div id="bar-16">
					</div>
					<div id="bar-17">
					</div>
					<div id="bar-18">
					</div>
					<div id="bar-19">
					</div>
					<div id="bar-20">
					</div>
					<div id="bar-21">
					</div>
					<div id="bar-22">
					</div>
					<div id="bar-23">
					</div>
					<div id="bar-24">
					</div>
					<div id="bar-25">
					</div>
				</div>

			</div>
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Plans & Sheets</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[27]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number cyan"><?php echo $timerManager->estimateNumber("Plans&Sheets",$timerArray[27]);?></span><br />
			<span class="result block caption">Estimated # of Plans & Sheets created in 10min</span><br />
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Detail Views</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[26]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number yellow-green"><?php echo $timerManager->estimateNumber("DetailViews",$timerArray[26]);?></span><br />
			<span class="result block caption">Estimated # of Detail Views created in 10min</span><br />
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Open / Save</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[28]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number magenta"><?php echo $timerManager->estimateNumber("OpenSave",$timerArray[28]);?></span><br />
			<span class="result block caption">Estimated # of Open/Save operations performed in 10min </span><br />
		</div>

		<div class="twelve columns"><br /><br />
			<hr />
		</div>
		<div class="four columns" style="height: 200px; border: 0px solid white;">
			<span class="result block title">Export DWF</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[30]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number yellow"><?php echo $timerManager->estimateNumber("ExportDWF",$timerArray[30]);?></span><br />
			<span class="result block caption">Estimated # of 3D DWF files exported in 10min</span><br />
		</div>
		<div class="four columns" style="height: 400px; border: 0px solid white;">
			<span class="result block title">Open 3D View</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[31]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number red" style=" padding-top:50px; "><?php echo $timerManager->estimateNumber("3DView",$timerArray[31]);?></span><br />
			<span class="result block caption">Estimated # of 3D Views opened in 10 min</span><br />
		</div>
		<div class="four columns" style="height: 400px; border: 0px solid white;">
			<span class="result block title">Adjust Types</span><br />
			<span class="result block timer"><?php echo $timerManager->toMins($timerArray[32]);?></span><br />
			<span class="result block caption">minutes</span><br /><br />
			<span class="result block number blue" style=" padding-top:50px; "><?php echo $timerManager->estimateNumber("AdjustTypes",$timerArray[32]);?></span><br />
			<span class="result block caption">Estimated # of Types adjusted in 10min</span><br />
		</div>
	</div><!-- container -->
	<script src="../../js/jquery.min.js" type="text/javascript"></script>
	<script src="../../js/jqbar.js" type="text/javascript"></script>
    <script type="text/javascript">

    $(document).ready(function () {

    $('#bar-1').jqbar({ label: '<?php echo $timerArray[1]; ?>', value: <?php echo $timerArray[1]; ?>, barColor: '#33ff99' });
    	
    $('#bar-2').jqbar({ label: '<?php echo $timerArray[2]; ?>', value: <?php echo $timerArray[2]; ?>, barColor: '#33ff99' });

	$('#bar-3').jqbar({ label: '<?php echo $timerArray[3]; ?>', value: <?php echo $timerArray[3]; ?>, barColor: '#33ff99' });

	$('#bar-4').jqbar({ label: '<?php echo $timerArray[4]; ?>', value: <?php echo $timerArray[4]; ?>, barColor: '#33ff99' });

	$('#bar-5').jqbar({ label: '<?php echo $timerArray[5]; ?>', value: <?php echo $timerArray[5]; ?>, barColor: '#33ff99' });

	$('#bar-6').jqbar({ label: '<?php echo $timerArray[6]; ?>', value: <?php echo $timerArray[6]; ?>, barColor: '#33ff99' });

	$('#bar-7').jqbar({ label: '<?php echo $timerArray[7]; ?>', value: <?php echo $timerArray[7]; ?>, barColor: '#33ff99' });

	$('#bar-8').jqbar({ label: '<?php echo $timerArray[8]; ?>', value: <?php echo $timerArray[8]; ?>, barColor: '#33ff99' });

	$('#bar-9').jqbar({ label: '<?php echo $timerArray[9]; ?>', value: <?php echo $timerArray[9]; ?>, barColor: '#33ff99' });

	$('#bar-10').jqbar({ label: '<?php echo $timerArray[10]; ?>', value: <?php echo $timerArray[10]; ?>, barColor: '#33ff99' });

	$('#bar-11').jqbar({ label: '<?php echo $timerArray[11]; ?>', value: <?php echo $timerArray[11]; ?>, barColor: '#33ff99' });

	$('#bar-12').jqbar({ label: '<?php echo $timerArray[12]; ?>', value: <?php echo $timerArray[12]; ?>, barColor: '#33ff99' });
		
	$('#bar-13').jqbar({ label: '<?php echo $timerArray[13]; ?>', value: <?php echo $timerArray[13]; ?>, barColor: '#33ff99' });

	$('#bar-14').jqbar({ label: '<?php echo $timerArray[14]; ?>', value: <?php echo $timerArray[14]; ?>, barColor: '#33ff99' });

	$('#bar-15').jqbar({ label: '<?php echo $timerArray[15]; ?>', value: <?php echo $timerArray[15]; ?>, barColor: '#33ff99' });

	$('#bar-16').jqbar({ label: '<?php echo $timerArray[16]; ?>', value: <?php echo $timerArray[16]; ?>, barColor: '#33ff99' });

	$('#bar-17').jqbar({ label: '<?php echo $timerArray[17]; ?>', value: <?php echo $timerArray[17]; ?>, barColor: '#33ff99' });

	$('#bar-18').jqbar({ label: '<?php echo $timerArray[18]; ?>', value: <?php echo $timerArray[18]; ?>, barColor: '#33ff99' });

	$('#bar-19').jqbar({ label: '<?php echo $timerArray[19]; ?>', value: <?php echo $timerArray[19]; ?>, barColor: '#33ff99' });

	$('#bar-20').jqbar({ label: '<?php echo $timerArray[20]; ?>', value: <?php echo $timerArray[20]; ?>, barColor: '#33ff99' });

	$('#bar-21').jqbar({ label: '<?php echo $timerArray[21]; ?>', value: <?php echo $timerArray[21]; ?>, barColor: '#33ff99' });

	$('#bar-22').jqbar({ label: '<?php echo $timerArray[22]; ?>', value: <?php echo $timerArray[22]; ?>, barColor: '#33ff99' });

	$('#bar-23').jqbar({ label: '<?php echo $timerArray[23]; ?>', value: <?php echo $timerArray[23]; ?>, barColor: '#33ff99' });

	$('#bar-24').jqbar({ label: '<?php echo $timerArray[24]; ?>', value: <?php echo $timerArray[24]; ?>, barColor: '#33ff99' });

	$('#bar-25').jqbar({ label: '<?php echo $timerArray[25]; ?>', value: <?php echo $timerArray[25]; ?>, barColor: '#33ff99' });

	});
	</script>
	<script src="../../js/highcharts.js"></script>
	
	<!-- //-->
	<!-- End Document
	================================================== -->
<?php }
		}?>	
	</body>
</html>