<?php

chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';
require_once 'vendor/autoload.php';
require_once 'controlSession.php';


use Zend\Session\Container;
/*

$email = $user;
// check @ login -> session start
*/
$UserSession = new UserSession(new Container('userSession'));

//$UserSession->save("email", $email);

$session_email = $UserSession->get("email");

if(empty($session_email)){

	echo "NO SESSION STARTED";
	exit;
}
//print_r($_SESSION);

set_time_limit(0);

require 'dbconfig.php';
require 'modelResults.php';


$testManager = new TestManager($db_config);

$timerManager = new TimerManager($db_config);

//get Session Data ID

if (isset($_GET['id'])) {
	$session_data_id = $_GET['id'];

	$timerArray = array();

	$timerManager->getTimers("timer_result", $session_data_id, $timerArray);

	$i = 1;
	$timerPartial = 0;
	while ($i <= 25){
		$timerPartial = $timerPartial + $timerArray[$i];
		$i++;
	}

}

$listTests_array = $testManager->listTests_BM($session_email);

if(isset($_GET['cn'])){
	$computer_name = $_GET['cn'];;
	$machineData = $testManager->getMachineData($session_email, $computer_name);


$listTestsByUserByMachine = $testManager->listTestsBU_BM($session_email,$computer_name); 


 if(isset($_GET['id'])){
			
			$session_data_id = $_GET['id'];
			include 'viewResultsPanel.php';
			
		} //end if id empty
		else {
			echo "User in Session: ".$UserSession->get("email")."<br />";
			echo "Select a Test";
			include 'viewTests.php';
			include 'viewResultsPanel.php';
			include 'viewFooter.php';
		}

			
	} //end if cn empty
	else {
		echo "User in Session: ".$UserSession->get("email")."<br />";
	 	echo "Select a Computer";
	 	include 'viewMachines.php';	 	
	 	include 'viewResultsPanel.php';
	 	include 'viewFooter.php';
	}
	

