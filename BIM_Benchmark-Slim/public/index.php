<?php
require '../vendor/autoload.php';

$app = new \Slim\Slim();

$app->config(
		array(
			'templates.path' => './templates'
			)
	);

$app->get('/userList', function () {
	include_once 'userList.php';
});

$app->get('/results/', function (){
	include_once 'controlResults.php';
});

	$app->get('/login/:user', function ($user){
		include_once 'login.php';
	});
	

/*
$app->get('/myResults/:user', function ($user){
	include_once 'benchmark_results.php';
}); 
 
$app->get('/', function () use ($app) {
	$app->render('home.phtml', array('nombre' => 'Jose'));
});

$app->get('/signup', function () use ($app) {
    include_once 'signup.php';
});

$app->get('/usuario/:usuario/estadistica/:estad', function ($usuario, $estad) use ($app){
	$app->render('usuario.phtml', array(
									'usuario' => $usuario,
									'estad' => $estad
									));
});

$app->get('/account/:usuario', function ($usuario) {
    include_once 'account.php';
});

$app->get('/results/:usuario', function ($usuario) use ($app) {
	$app->render('results.phtml', array(
									'usuario' => $usuario
									));
});
*/
$app->run();