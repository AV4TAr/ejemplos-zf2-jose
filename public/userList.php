<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';


//-------------------------------------------------------start comment on server upload
// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';

//-------------------------------------------------------end comment on server upload
use Zend\Db\Sql\Select;
set_time_limit(0);

class userManager
{
	private $json_data;
	private $table;
	private $db_config;
	private $sessionId;
	private $theTable;
	private $jsonExtract;
	private $adapter;

	public function __construct($db_config)
	{
		$this->db_config=$db_config;
		$this->adapter=new Zend\Db\Adapter\Adapter($this->db_config);

	}

	public function listUsers($theTable)
	{
		$projectTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		//$rowset = $projectTable->select(array('session_data_id' => $session_data_id));
		/*		*/
		$rowset = $projectTable->select(function (Select $select) {
			$select->where->like('email', '%%');
			//$select->order('name ASC')->limit(2);
		});

			foreach ($rowset as $projectRow) {
				echo "<a href='benchmark_results.php?u=".$projectRow['email']."'>".$projectRow['email']."</a>->".$projectRow['name']."<br />";
			}
		
	}

}

require 'dbconfig.php';

$userManager = new userManager($db_config);

$userManager->listUsers("user");