<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';
set_time_limit(0);
class JsonImporter
{
	private $json_data;
	private $table;
	private $db_config;
	private $sessionId;
	private $theTable;
	private $jsonExtract;
	private $adapter;
	
	public function __construct($db_config)
	{
		$this->db_config=$db_config;
		$this->adapter=new Zend\Db\Adapter\Adapter($this->db_config);
		
	}
	/*
	public function process()
	{
		$this->json_data=$
	}
	*/
	public function json2dB($theTable, array $jsonExtract)
	{
		$table = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$tableMeta = new Zend\Db\TableGateway\TableGateway($theTable, $this->adapter, new Zend\Db\TableGateway\Feature\MetadataFeature());
		$cols = array();
		$cols = $tableMeta->getColumns();
		array_shift($cols);
		if ($theTable == "session_data") {
			
		}else {
			//$jsonExtract['Session'] = $this->getLastIndex();
			$jsonExtract['Session'] = $this->sessionId;
		}
		echo "<br /><br />$theTable: json=>".count($jsonExtract)." vs cols=>".count($cols)."<br />";
		echo var_dump($jsonExtract)."<br /><br />";
		echo var_dump($cols)."<br />";
		
		$list = array_combine($cols, array_values($jsonExtract));
		$table->insert($list);
		if ($theTable == "session_data") {
		$this->sessionId = $table->getLastInsertValue();
		}
	}
	
}

require 'dbconfig';

//$json=utf8_encode(file_Get_contents("public/BenchmarkData_rev.json"));
$json=utf8_encode(file_Get_contents("public/BenchmarkData.json"));
// Decode JSON objects as PHP objects
$jsonData = Zend\Json\Json::decode($json, Zend\Json\Json::TYPE_OBJECT);


$jsonFields_Tables = array (
		"Session" => array (
				0 => "session_data",
				1 => array_slice(get_object_vars($jsonData), 0, 6)
		),
		"Machine" => array (
				0 => "hw_spec_general",
				1 => array_slice(get_object_vars($jsonData->Machine), 0, 12)
		),
		/*
		"Timers" => array (
				0 => "timer_result",
				1 => get_object_vars($jsonData->Timers[0])
		),

		"Video" => array (
				0 =>"hw_spec_video",
				1 =>get_object_vars($jsonData->Machine->Video[0])
		),
		"Processors" => array (
				0 => "hw_spec_processor",
				1 => get_object_vars($jsonData->Machine->Processors[0])
		),
		"RAM" => array (
				0 => "hw_spec_ram",
				1 => get_object_vars($jsonData->Machine->RAM[0])
		),
		"HD" => array (
				0 => "hw_spec_hd",
				1 => get_object_vars($jsonData->Machine->HD[0])
		),
		"DrivePartitions" => array (
				0 => "hw_spec_drive_partition",
				1 => get_object_vars($jsonData->Machine->DrivePartitions[0])
		),
		*/
);

function getJsonLvls($fieldName, $upperArray, $tableName, &$mainArray)
{
	$subElemCount = count($upperArray);
	if($subElemCount >= 0){
		foreach ($upperArray as $key => $val){
			//echo "<br />$key: ";
			/*
			$fieldName.$key = array($tableName, $upperArray[$key]);
			array_push($jsonFields_Tables, $fieldName[$key]);
			
			$fieldName = array($tableName, $upperArray[0]);*/
			$mainArray[$fieldName.$key]=array($tableName, (get_object_vars($upperArray[$key])));
		}
	}
	
}


getJsonLvls("Timers", $jsonData->Timers, "timer_result", $jsonFields_Tables);
getJsonLvls("Video", $jsonData->Machine->Video, "hw_spec_video", $jsonFields_Tables);
getJsonLvls("Processors", $jsonData->Machine->Processors, "hw_spec_processor", $jsonFields_Tables);
getJsonLvls("RAM", $jsonData->Machine->RAM, "hw_spec_ram", $jsonFields_Tables);
getJsonLvls("HD", $jsonData->Machine->HD, "hw_spec_hd", $jsonFields_Tables);
getJsonLvls("DrivePartitions", $jsonData->Machine->DrivePartitions, "hw_spec_drive_partition", $jsonFields_Tables);

echo "------------------------------<br />".var_dump($jsonFields_Tables)."<br />";
/*
getJsonLvls($jsonData->Machine->RAM);

getJsonLvls($jsonData->Machine->Video);
*/

function convertBool(&$items){
	foreach ($items as $key=>$value){
		if($value==false){
			$items->$key = 0;
		}elseif ($value==true){
			$items->$key = 1;
		}
	}
}

//array_walk_recursive($jsonFields_Tables,'convertBool');


$session = $jsonData->Session;

$JsonImporter = new JsonImporter($db_config);


/*
var_dump($jsonFields_Tables["Session"][0]);
echo "<br />";
var_dump($jsonFields_Tables["Session"][1]);
echo "<br />";
var_dump($jsonFields_Tables["Processors"][0]);
echo "<br />";
var_dump($jsonFields_Tables["Processors"][1]);
echo "<br />";
var_dump($jsonFields_Tables["Machine"][0]);
echo "<br />";
var_dump($jsonFields_Tables["Machine"][1]);
echo "<br />";
var_dump($jsonFields_Tables["Video"][0]);
echo "<br />";
var_dump($jsonFields_Tables["Video"][1]);
echo "<br />";
var_dump($jsonFields_Tables["HD"][0]);
echo "<br />";
var_dump($jsonFields_Tables["HD"][1]);
echo "<br />";
var_dump($jsonFields_Tables["DrivePartitions"][0]);
echo "<br />";
var_dump($jsonFields_Tables["DrivePartitions"][1]);
//$JsonImporter->json2dB($table_session_data, $session_data);
//$JsonImporter->json2dB($table_hw_spec_processor, $hw_spec_processor);
*/

foreach($jsonFields_Tables as $key => $val){
	$JsonImporter->json2dB($val[0], $val[1]);
}




// b DS

//$registros = $tableTest->select(array('id' => 1));

//$registro = $registros->current();

//$registro->name = "jose";
//$registro->save();


//Objecto ->  Zend\Db\TableGateway\TableGateway -> mapea una tabla
//Select -> Coleccio de Zend\Db\RowGateway -> Zend\Db\ResultSet
//Registro -> Zend\Db\RowGateway -> mapeo de un registro de la tabla