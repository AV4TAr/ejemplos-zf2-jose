<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';


//http://framework.zend.com/manual/2.2/en/index.html#zend-db

$configArray =array(
		
		//'driver' => 'Mysqli',
		//'database' => 'creatio_caseTest',
		//'username' => 'creatio_caseTest',
		//'password' => 'c4s3T3st',
		//'hostname' => '181.164.212.132'
				
		'driver' => 'Mysqli',
		'database' => 'creatio_caseTest',
		'username' => 'root',
		'password' => 'LQSyM5',
		'hostname' => 'localhost'
);
$adapter = new Zend\Db\Adapter\Adapter($configArray);

$tableTest = new Zend\Db\TableGateway\TableGateway('session_data', $adapter);


//$tableTest->insert(array("email" => "d.rudder@case-inc.com", "addinVer"=> "v2013.10.14.0", "revitBuild" => "20130709_2115(x64)", "revitName" => "Autodesk Revit 2014", "revitLanguage" => "English_USA", "session_id" => "f4ab4011-e4d7-466e-9581-d8510082feab"));

$table = new Zend\Db\TableGateway\TableGateway('session_data', $adapter, new Zend\Db\TableGateway\Feature\MetadataFeature());
$cols = $table->getColumns();
$cols_no_id = array_shift($cols);

/*
$cols_implode = "\"" . implode("\",\"", $cols) . "\"";
$cols = explode(",", $cols_implode);
*/


//var_dump($cols);

echo "<br />";

$json=utf8_encode(file_Get_contents("public/BenchmarkData.json"));

var_dump($cols);
$arFromJson = array(
		"Email" => "j.torres@example.com",
		"AddInVersion" => "v2013.10.14.0",
		"RevitBuild" => "20130709_2115(x64)",
		"RevitName" => "Autodesk Revit 2014",
		"RevitLanguage" => "English_USA",
		"Session" => "f4ab4011-e4d7-466e-9581-d8510082feab");
/*
$arFromJson_imploded = "\"" . implode("\",\"", $arFromJson) . "\"";
$arFromJson = explode(",", $arFromJson_imploded);
*/
//var_dump($arFromJson);
echo "<br /><br /><br />";
$list = array_combine($cols, array_values($arFromJson));
//array_walk($list, create_function('&$str', '$str = "\"$str\"";'));
var_dump($list);
echo "<br />";
$theArray = '';
foreach ($list as $k => $v){
		$theArray .= "$k => $v, ";
	}
echo "<br />ECHO: ".$theArray."<br />";

$tableTest->insert($list);
echo "<br />";
//$arrayTest = array("email" => "d.rudder@case-inc.com", "addinVer"=> "v2013.10.14.0", "revitBuild" => "20130709_2115(x64)", "revitName" => "Autodesk Revit 2014", "revitLanguage" => "English_USA", "session_id" => "f4ab4011-e4d7-466e-9581-d8510082feab");
//$tableTest->insert(array("email" => "d.rudder@case-inc.com", "addinVer"=> "v2013.10.14.0", "revitBuild" => "20130709_2115(x64)", "revitName" => "Autodesk Revit 2014", "revitLanguage" => "English_USA", "session_id" => "f4ab4011-e4d7-466e-9581-d8510082feab"));
//$tableTest->insert(array($arrayTest));

// Decode JSON objects as PHP objects
$phpNative = Zend\Json\Json::decode($json, Zend\Json\Json::TYPE_OBJECT);
echo $phpNative->Machine->Win_UserName;
echo $phpNative->Machine->Video[0]->Availability;
echo $phpNative->Timers[1]->Name;
$otherArray = array();
$otherArray = $phpNative->Timers[1];
//var_dump($phpNative);
var_dump($otherArray);

//$registros = $tableTest->select(array('id' => 1));

//$registro = $registros->current();

//$registro->name = "jose";
//$registro->save();


//Objecto ->  Zend\Db\TableGateway\TableGateway -> mapea una tabla
//Select -> Coleccio de Zend\Db\RowGateway -> Zend\Db\ResultSet
//Registro -> Zend\Db\RowGateway -> mapeo de un registro de la tabla