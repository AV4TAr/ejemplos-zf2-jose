<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';


//-------------------------------------------------------start comment on server upload
// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';

//-------------------------------------------------------end comment on server upload
use Zend\Db\Sql\Select;
set_time_limit(0);

class userManager
{
	private $json_data;
	private $table;
	private $db_config;
	private $sessionId;
	private $theTable;
	private $jsonExtract;
	private $adapter;

	public function __construct($db_config)
	{
		$this->db_config=$db_config;
		$this->adapter=new Zend\Db\Adapter\Adapter($this->db_config);

	}

	public function listUsers($theTable)
	{
		$projectTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		//$rowset = $projectTable->select(array('session_data_id' => $session_data_id));
		/*		*/
		$rowset = $projectTable->select(function (Select $select) {
			$select->where->like('email', '%%');
			//$select->order('name ASC')->limit(2);
		});

			foreach ($rowset as $projectRow) {
				echo "<a href='testResults.php?u=".$projectRow['email']."'>".$projectRow['email']."</a>->".$projectRow['name']."<br />";
			}
		
	}
	
	public function getTimers($theTable, $session_data_id,&$timer)
	{
		/*
		 $table = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$row = new Zend\Db\TableGateway\Feature\RowGatewayFeature;


		$table = new Zend\Db\TableGateway\TableGateway('timer_result', $this->$adapter, new RowGatewayFeature('session_data_id'));
		$results = $table->select(array('sessio_data_id' => 2));


		$results = $table->select(array('session_data_id' => 2));

		$artistRow = $results->current();

		$row->select("WHERE 'session_data_id' = 2");
		var_dump($row);
		//$artistRow->name = 'New Name';
		//$artistRow->save();

		*/
		$projectTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$rowset = $projectTable->select(array('session_data_id' => $session_data_id));
		$jqbar_data = array();
		$i = 1;
		foreach ($rowset as $projectRow) {
			$timer[$i] = $projectRow['seconds'];
			//$jqbar_data[$i] = "label: '".$projectRow['seconds']."', value: ".$projectRow['seconds'].", barColor: '#33ff99'";
			$i++;
		}

		// or, when expecting a single row:
		/*
		 $artistTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$rowset = $artistTable->select(array('session_data_id' => 2));
		$artistRow = $rowset->current();

		var_dump($artistRow);

		$table = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$registros = $table->select(array('session_data_id' => 1));

		$registro = $registros->current();

		$registro_1 = $registro['seconds'];
		echo $registro_1;
		*/

	}

}

require 'dbconfig.php';

$userManager = new userManager($db_config);

$userManager->listUsers("user");