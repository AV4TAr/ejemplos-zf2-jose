<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';


//-------------------------------------------------------start comment on server upload
// Run the application! 
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';

//-------------------------------------------------------end comment on server upload

set_time_limit(0);

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container; // We need this when using sessions
use Zend\Db\Sql;

class UserController extends AbstractActionController {
	public function setSessionData() {
		// Store username in session
		$user_session = new Container('user_email');
		$user_session->useremail = $_GET['u'];

		//return $this->redirect()->toRoute('welcome');
	}

	public function getSessionData() {
		// Retrieve username from session
		$user_session = new Container('user_email');
		$user_email = $user_session->useremail; 
		return $user_email;
	}
}
$session = new UserController();
$session->setSessionData();

$session_email = $session->getSessionData();
echo "session email: ".$session_email."<br/>";

class TestManager
{
	private $table;
	private $db_config;
	private $session_email;
	private $theTable;
	private $adapter;
	
	public function __construct($db_config)
	{
		$this->db_config=$db_config;
		$this->adapter=new Zend\Db\Adapter\Adapter($this->db_config);
		
	}
	
	public function listTests($session_email, $theTable)
	{
		$testTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		$rowset = $testTable->select(array('email' => $session_email));
		/*		*/
		//$rowset = $testTable->select(function (Select $select) {
			//$select->where->like('email', '%%');
			//$select->order('name ASC')->limit(2);
		//});

			foreach ($rowset as $testRow) {
				echo "<a href='results.php?u=".$testRow['email']."'>".$testRow['email']."</a>->".$testRow['session_id']."<br />";
			}
		
	}	

	public function listTestsByUser($session_email)
	{
		/*
		//$db = Zend\Db\Sql;
		$select = new Zend\Db\Sql\Select($this->adapter);
		
		$select->select()
		->from('session_data',
				array('session_id', 'revitBuild', 'revitName'))
				->where('email =', $session_email);
		*/
		//$testTable = new Zend\Db\TableGateway\TableGateway($theTable,$this->adapter);
		//$theTables = array($theTable, 'hw_spec_general');
		
		$sql = new Zend\Db\Sql\Sql($this->adapter);
		
		$select = $sql->select();
		echo "session_email: ".$session_email;
		$select->from(array('session_data' => 'session_data'))  // base table
		->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
				'session_data.id = hw_spec_general.session_data_id')// join expression
		->group('hw_spec_general.computer_name')
		->where->like('session_data.email' , $session_email);
		       
		$selectString = $sql->getSqlStringForSqlObject($select);
		$theadapter = $this->adapter;
		$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);
		//->join('hw_spec_general', 'session_data.id=hw_spec_general.session_data_id', array('computer_name'));
		
		//$resultSet = $testTable->selectWith($select);
		
		//return $resultSet;
		//$rowset = $testTable->select()
		foreach ($results as $testRow) {
			echo "<br/><a href='results.php?u=".$testRow['email']."'>".$testRow['email']."</a>->".$testRow['computer_name']."--".$testRow['os_name']."<br />";
		
		}
		
	}
	public function listTestsByUserByMachine($session_email)
	{
	
		$sql = new Zend\Db\Sql\Sql($this->adapter);
	
		$select = $sql->select();
		//echo "session_email: ".$session_email;
		$select->from(array('session_data' => 'session_data'))  // base table
		->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
				'session_data.id = hw_spec_general.session_data_id')// join expression
				->group('hw_spec_general.computer_name')
				->where->like('session_data.email' , $session_email);
		 
		$selectString = $sql->getSqlStringForSqlObject($select);
		$theadapter = $this->adapter;
		$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);

		foreach ($results as $testRow) {
			//echo "<br/><a href='results.php?u=".$testRow['email']."'>".$testRow['email']."</a>->".$testRow['computer_name']."--".$testRow['os_name']."<br />";
	
			
			$sql2 = new Zend\Db\Sql\Sql($this->adapter);
			
			$select2 = $sql2->select();
			echo "Computer Name: ".$testRow['computer_name'];
			$thisComputerName = $testRow['computer_name'];
			$select2->from(array('session_data' => 'session_data'))  // base table
			->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
					'session_data.id = hw_spec_general.session_data_id')// join expression
					//->group('hw_spec_general.computer_name')
					->where(array('hw_spec_general.computer_name' => $thisComputerName, 'session_data.email' => $session_email));
					//->AND('session_data.email = $session_email');
				
			$selectString2 = $sql2->getSqlStringForSqlObject($select2);
			$theadapter = $this->adapter;
			$results2 = $this->adapter->query($selectString2, $theadapter::QUERY_MODE_EXECUTE);
			
			foreach ($results2 as $testRow2) {
				echo "<br/><a href='benchmark_results.php?u=".$testRow2['email']."&id=".$testRow2['session_data_id']."'>".$testRow2['computer_name']."</a>->".$testRow2['os_name']."<br />";
			}
			
		}
	
	}
	public function listTests_BM($session_email)
	{
	
		$sql = new Zend\Db\Sql\Sql($this->adapter);
	
		$select = $sql->select();
		//echo "session_email: ".$session_email;
		$select->from(array('session_data' => 'session_data'))  // base table
		->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
				'session_data.id = hw_spec_general.session_data_id')// join expression
				->group('hw_spec_general.computer_name')
				->where->like('session_data.email' , $session_email);
			
		$selectString = $sql->getSqlStringForSqlObject($select);
		$theadapter = $this->adapter;
		$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);
	
		foreach ($results as $row) {
		echo "<li><a href='".$_SERVER['PHP_SELF']."?u=".$row['email']."&id=".$row['session_data_id']."&cn=".$row['computer_name']."'><span class='grey'>".$row['session_data_id']."</span><br /><br /><span class='time'><span style='float: left; text-align: left;'>Time</span><span style='float: right; text-align: right;'>13:59min<span></span></a></li>";

				
		}
	
	}
	public function listTestsBU_BM($session_email, $computer_name)
	{
	
		$sql = new Zend\Db\Sql\Sql($this->adapter);
				
			$select = $sql->select();
			$select->from(array('session_data' => 'session_data'))  // base table
			->join(array('hw_spec_general' => 'hw_spec_general'),     // join table with alias
					'session_data.id = hw_spec_general.session_data_id')// join expression
					//->group('hw_spec_general.computer_name')
			->where(array('hw_spec_general.computer_name' => $computer_name, 'session_data.email' => $session_email));
			//->AND('session_data.email = $session_email');
	
			$selectString = $sql->getSqlStringForSqlObject($select);
			$theadapter = $this->adapter;
			$results = $this->adapter->query($selectString, $theadapter::QUERY_MODE_EXECUTE);
				
			foreach ($results as $row) {
				echo "<li><a href='".$_SERVER['PHP_SELF']."?u=".$row['email']."&id=".$row['session_data_id']."'><span class='grey'>".$row['session_data_id']."</span><br /><br /><span class='time'><span style='float: left; text-align: left;'>Time</span><span style='float: right; text-align: right;'>13:59min<span></span></a></li>";
					
				//echo "<br/><a href='benchmark_results.php?u=".$testRow2['email']."&id=".$testRow2['session_data_id']."'>".$testRow2['computer_name']."</a>->".$testRow2['os_name']."<br />";
			}
				
		}
	
}
require 'dbconfig.php';

$testManager = new TestManager($db_config);
