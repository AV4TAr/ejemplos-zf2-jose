<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';


//http://framework.zend.com/manual/2.2/en/index.html#zend-db


$json=utf8_encode(file_Get_contents("public/BenchmarkData.json"));

//var_dump($cols);
/*
$arFromJson = array(
		"Email" => "j.torres@example.com",
		"AddInVersion" => "v2013.10.14.0",
		"RevitBuild" => "20130709_2115(x64)",
		"RevitName" => "Autodesk Revit 2014",
		"RevitLanguage" => "English_USA",
		"Session" => "f4ab4011-e4d7-466e-9581-d8510082feab");
*/


// Decode JSON objects as PHP objects
//$jsonData = Zend\Json\Json::decode($json, Zend\Json\Json::TYPE_OBJECT);
$jsonData = Zend\Json\Json::decode($json, Zend\Json\Json::TYPE_OBJECT);
$session_data = array();
$session_id = $jsonData->Session;
$table_session_data = "session_data";
$session_data = array(
		"Email" => $jsonData->Email,
		"AddInVersion" => $jsonData->AddInVersion,
		"RevitBuild" => $jsonData->RevitBuild,
		"RevitName" => $jsonData->RevitName,
		"RevitLanguage" => $jsonData->RevitLanguage,
		"Session" => $jsonData->Session);

$table_hw_spec_processor = "hw_spec_processor";
//$hw_spec_processor = array();
$hw_spec_processor = get_object_vars($jsonData->Machine->Processors[0]);

echo "<br />Dynamically assigned original: "; var_dump($hw_spec_processor);
$session = $jsonData->Session;
//array_push($hw_spec_processor,'Session',$jsonData->Session);
//$addSession = array();
//$addSession['Session'] = $session;
//$hw_spec_processor=array_merge((array)$hw_spec_processor, (array)$addSession);	
//echo "<br />Dynamically assigned: "; var_dump($hw_spec_processor);
/*
echo $phpNative->Machine->Win_UserName;
echo $phpNative->Machine->Video[0]->Availability;
echo $phpNative->Timers[1]->Name;
$otherArray = array();
$otherArray = $phpNative->Timers[1];
*/
//var_dump($phpNative);
//var_dump($otherArray);

/* FUNCTION */
function json2dB($sessionId, $theTable, array $jsonExtract)
{	
	$cols = array();
	//echo $theTable." and ".$jsonExtract['AddInVersion']."<br />";
	//echo "testValue = ".$jsonExtract['Email'];
	$configArray =array(
			'driver' => 'Mysqli',
			'database' => 'creatio_caseTest',
			'username' => 'root',
			'password' => 'LQSyM5',
			'hostname' => 'localhost'
	);
	$adapter = new Zend\Db\Adapter\Adapter($configArray);
	$table = new Zend\Db\TableGateway\TableGateway($theTable, $adapter);
	$tableMeta = new Zend\Db\TableGateway\TableGateway($theTable, $adapter, new Zend\Db\TableGateway\Feature\MetadataFeature());
	$cols = $tableMeta->getColumns();
	$cols_no_id = array_shift($cols);
		if ($theTable == "session_data") {

		}else {
			//array_push($jsonExtract,'Session',$sessionId);
			//$hw_spec_processor=array_merge((array)$hw_spec_processor, (array)$addSession);
			$jsonExtract['Session'] = $sessionId;
			
			echo "if ok for $theTable<br/>";
		}
	echo "-------------------------------<br /><br />";
	var_dump($jsonExtract);
	echo "<br /><br /><br />";
	var_dump($cols);
	echo "<br /><br />$theTable: ".count($jsonExtract)." vs ".count($cols);
	echo "<br /><br /><br />";
	$list = array_combine($cols, array_values($jsonExtract));
	$table->insert($list);
}
//json2dB($session_id, $table_session_data, $session_data);
json2dB($session_id, $table_hw_spec_processor, $hw_spec_processor);


//$cols['Session'] = $session_id;
//var_dump($cols);

//$registros = $tableTest->select(array('id' => 1));

//$registro = $registros->current();

//$registro->name = "jose";
//$registro->save();


//Objecto ->  Zend\Db\TableGateway\TableGateway -> mapea una tabla
//Select -> Coleccio de Zend\Db\RowGateway -> Zend\Db\ResultSet
//Registro -> Zend\Db\RowGateway -> mapeo de un registro de la tabla