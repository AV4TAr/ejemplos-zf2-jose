CREATE DATABASE  IF NOT EXISTS `creatio_casetest` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `creatio_casetest`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: creatio_casetest
-- ------------------------------------------------------
-- Server version	5.6.14

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hw_spec_drive_partition`
--

DROP TABLE IF EXISTS `hw_spec_drive_partition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_drive_partition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `available_free_space` double DEFAULT NULL,
  `total_size` double DEFAULT NULL,
  `drive_format` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_drive_partition_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_drive_partition_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_drive_partition`
--

LOCK TABLES `hw_spec_drive_partition` WRITE;
/*!40000 ALTER TABLE `hw_spec_drive_partition` DISABLE KEYS */;
INSERT INTO `hw_spec_drive_partition` VALUES (1,'C:\\',27138199552,240054763520,'NTFS',1),(2,'D:\\',229111668736,640124157952,'NTFS',1),(3,'F:\\',168313901056,1000202039296,'NTFS',1),(4,'G:\\',248435077120,1000202039296,'NTFS',1),(5,'O:\\',508684492800,1000202039296,'NTFS',1),(6,'C:\\',27138199552,240054763520,'NTFS',2),(7,'D:\\',229111668736,640124157952,'NTFS',2),(8,'F:\\',168313901056,1000202039296,'NTFS',2),(9,'G:\\',248435077120,1000202039296,'NTFS',2),(10,'O:\\',508684492800,1000202039296,'NTFS',2),(11,'C:\\',27138199552,240054763520,'NTFS',3),(12,'D:\\',229111668736,640124157952,'NTFS',3),(13,'F:\\',168313901056,1000202039296,'NTFS',3),(14,'G:\\',248435077120,1000202039296,'NTFS',3),(15,'O:\\',508684492800,1000202039296,'NTFS',3);
/*!40000 ALTER TABLE `hw_spec_drive_partition` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_general`
--

DROP TABLE IF EXISTS `hw_spec_general`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_general` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `os_name` varchar(45) DEFAULT NULL,
  `os_version` varchar(45) DEFAULT NULL,
  `os_bits` int(25) DEFAULT NULL,
  `computer_name` varchar(45) DEFAULT NULL,
  `available_physical_memory` double DEFAULT NULL,
  `available_virtual_memory` double DEFAULT NULL,
  `total_pysical_memory` double DEFAULT NULL,
  `total_virtual_memory` double DEFAULT NULL,
  `number_processors` int(11) DEFAULT NULL,
  `number_logical_processors` int(11) DEFAULT NULL,
  `pc_system_type` int(25) DEFAULT NULL,
  `win_user_name` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='	';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_general`
--

LOCK TABLES `hw_spec_general` WRITE;
/*!40000 ALTER TABLE `hw_spec_general` DISABLE KEYS */;
INSERT INTO `hw_spec_general` VALUES (1,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',1),(2,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',2),(3,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',3),(4,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',4),(5,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',5),(6,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',6),(7,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',7),(8,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',8),(9,'Microsoft Windows 7 Professional ','6.1.7601.65536',64,'MASTERDONPC',25982349312,8793502195712,34318561280,8796092891136,1,8,1,'MasterDon',9);
/*!40000 ALTER TABLE `hw_spec_general` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_hd`
--

DROP TABLE IF EXISTS `hw_spec_hd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_hd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `model` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `serial_number` varchar(45) DEFAULT NULL,
  `interface_type` varchar(45) DEFAULT NULL,
  `bytes_per_sector` int(25) DEFAULT NULL,
  `compression_method` varchar(45) DEFAULT NULL,
  `media_type` varchar(45) DEFAULT NULL,
  `partitions` int(11) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `total_cylinders` int(11) DEFAULT NULL,
  `total_heads` int(11) DEFAULT NULL,
  `total_sectors` int(11) DEFAULT NULL,
  `total_tracks` int(11) DEFAULT NULL,
  `tracks_per_cylinder` int(11) DEFAULT NULL,
  `default_block_size` int(25) DEFAULT NULL,
  `firmware_revision` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_hd_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_hd_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_hd`
--

LOCK TABLES `hw_spec_hd` WRITE;
/*!40000 ALTER TABLE `hw_spec_hd` DISABLE KEYS */;
INSERT INTO `hw_spec_hd` VALUES (1,'\\\\.\\PHYSICALDRIVE0','ST3640323AS ATA Device','(Standard disk drives)','Disk drive',640132416000,'2020202020202020202020205639304b42414158','IDE',512,'','Fixed hard disk media',1,'OK',77825,255,1250258625,19845375,255,0,'SD13',1),(2,'\\\\.\\PHYSICALDRIVE2','INTEL SS DSC2CW240A3 SATA Disk Device','(Standard disk drives)','Disk drive',240054796800,'CVCV303104VM240CGN  ','IDE',512,'','Fixed hard disk media',1,'OK',29185,255,468857025,7442175,255,0,'400i',1),(3,'\\\\.\\PHYSICALDRIVE3','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'S246J9HB906470      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',1),(4,'\\\\.\\PHYSICALDRIVE4','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'S246J9HB906476      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',1),(5,'\\\\.\\PHYSICALDRIVE1','ST1000NM 0011 SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'            Z1N3JETC','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'SN03',1),(6,'\\\\.\\PHYSICALDRIVE0','ST3640323AS ATA Device','(Standard disk drives)','Disk drive',640132416000,'2020202020202020202020205639304b42414158','IDE',512,'','Fixed hard disk media',1,'OK',77825,255,1250258625,19845375,255,0,'SD13',2),(7,'\\\\.\\PHYSICALDRIVE2','INTEL SS DSC2CW240A3 SATA Disk Device','(Standard disk drives)','Disk drive',240054796800,'CVCV303104VM240CGN  ','IDE',512,'','Fixed hard disk media',1,'OK',29185,255,468857025,7442175,255,0,'400i',2),(8,'\\\\.\\PHYSICALDRIVE3','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'S246J9HB906470      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',2),(9,'\\\\.\\PHYSICALDRIVE4','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'S246J9HB906476      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',2),(10,'\\\\.\\PHYSICALDRIVE1','ST1000NM 0011 SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'            Z1N3JETC','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'SN03',2),(11,'\\\\.\\PHYSICALDRIVE0','ST3640323AS ATA Device','(Standard disk drives)','Disk drive',640132416000,'2020202020202020202020205639304b42414158','IDE',512,'','Fixed hard disk media',1,'OK',77825,255,1250258625,19845375,255,0,'SD13',3),(12,'\\\\.\\PHYSICALDRIVE2','INTEL SS DSC2CW240A3 SATA Disk Device','(Standard disk drives)','Disk drive',240054796800,'CVCV303104VM240CGN  ','IDE',512,'','Fixed hard disk media',1,'OK',29185,255,468857025,7442175,255,0,'400i',3),(13,'\\\\.\\PHYSICALDRIVE3','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'S246J9HB906470      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',3),(14,'\\\\.\\PHYSICALDRIVE4','SAMSUNG HD103SJ SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'S246J9HB906476      ','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'1AJ1',3),(15,'\\\\.\\PHYSICALDRIVE1','ST1000NM 0011 SATA Disk Device','(Standard disk drives)','Disk drive',1000202273280,'            Z1N3JETC','IDE',512,'','Fixed hard disk media',1,'OK',121601,255,1953520065,31008255,255,0,'SN03',3);
/*!40000 ALTER TABLE `hw_spec_hd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_processor`
--

DROP TABLE IF EXISTS `hw_spec_processor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_processor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `current_clock_speed` int(25) DEFAULT NULL,
  `load_percentage` int(25) DEFAULT NULL,
  `l2_cache_speed` int(25) DEFAULT NULL,
  `l3_cache_speed` int(25) DEFAULT NULL,
  `l2_cache` int(25) DEFAULT NULL,
  `l3_cache` int(25) DEFAULT NULL,
  `max_clock_speed` int(25) DEFAULT NULL,
  `number_cores` int(11) DEFAULT NULL,
  `number_logical_processors` int(11) DEFAULT NULL,
  `current_voltage` int(25) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_processor_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_processor_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_processor`
--

LOCK TABLES `hw_spec_processor` WRITE;
/*!40000 ALTER TABLE `hw_spec_processor` DISABLE KEYS */;
INSERT INTO `hw_spec_processor` VALUES (1,'AMD FX(tm)-8320 Eight-Core Processor         ','AuthenticAMD',3500,5,0,0,0,0,3500,4,8,13,1),(2,'AMD FX(tm)-8320 Eight-Core Processor         ','AuthenticAMD',3500,5,0,0,0,0,3500,4,8,13,2),(3,'AMD FX(tm)-8320 Eight-Core Processor         ','AuthenticAMD',3500,5,0,0,0,0,3500,4,8,13,3);
/*!40000 ALTER TABLE `hw_spec_processor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_ram`
--

DROP TABLE IF EXISTS `hw_spec_ram`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_ram` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `manufacturer` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `capacity` double DEFAULT NULL,
  `data_width` int(25) DEFAULT NULL,
  `form_factor` int(25) DEFAULT NULL,
  `interleave_data_depth` int(25) DEFAULT NULL,
  `interleave_position` int(25) DEFAULT NULL,
  `memory_type` int(25) DEFAULT NULL,
  `speed` int(25) DEFAULT NULL,
  `total_width` int(25) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_ram_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_ram_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_ram`
--

LOCK TABLES `hw_spec_ram` WRITE;
/*!40000 ALTER TABLE `hw_spec_ram` DISABLE KEYS */;
INSERT INTO `hw_spec_ram` VALUES (1,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,1),(2,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,1),(3,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,1),(4,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,1),(5,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,2),(6,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,2),(7,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,2),(8,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,2),(9,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,3),(10,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,3),(11,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,3),(12,'Physical Memory','Undefined         ','Physical Memory',8589934592,64,8,0,0,0,667,64,3);
/*!40000 ALTER TABLE `hw_spec_ram` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hw_spec_video`
--

DROP TABLE IF EXISTS `hw_spec_video`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hw_spec_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accelerator_capabilities` varchar(45) DEFAULT NULL,
  `adapter_compatibility` varchar(45) DEFAULT NULL,
  `adapter_dac_type` varchar(45) DEFAULT NULL,
  `adapter_ram` int(11) DEFAULT NULL,
  `availability` int(11) DEFAULT NULL,
  `capability_descriptions` text,
  `caption` varchar(45) DEFAULT NULL,
  `color_table_entries` int(25) DEFAULT NULL,
  `config_manager_error_code` int(25) DEFAULT NULL,
  `config_manager_user_config` varchar(10) DEFAULT NULL,
  `creation_class_name` varchar(45) DEFAULT NULL,
  `current_bits_per_pixel` int(25) DEFAULT NULL,
  `current_horizontal_resolution` int(25) DEFAULT NULL,
  `current_number_colors` double DEFAULT NULL,
  `current_number_columns` int(25) DEFAULT NULL,
  `current_number_rows` int(25) DEFAULT NULL,
  `current_refresh_rate` int(25) DEFAULT NULL,
  `current_scan_mode` int(25) DEFAULT NULL,
  `current_vertical_resolution` int(25) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `device_id` varchar(45) DEFAULT NULL,
  `device_specific_pens` int(25) DEFAULT NULL,
  `dither_type` int(25) DEFAULT NULL,
  `driver_date` date DEFAULT NULL,
  `driver_version` varchar(45) DEFAULT NULL,
  `error_cleared` tinyint(1) DEFAULT NULL,
  `error_description` varchar(45) DEFAULT NULL,
  `icm_intent` int(25) DEFAULT NULL,
  `icm_method` int(25) DEFAULT NULL,
  `inf_file_name` varchar(45) DEFAULT NULL,
  `inf_section` varchar(45) DEFAULT NULL,
  `install_date` date DEFAULT NULL,
  `installed_display_drivers` text,
  `last_error_code` int(25) DEFAULT NULL,
  `max_memory_supported` int(25) DEFAULT NULL,
  `max_number_controlled` int(25) DEFAULT NULL,
  `max_refresh_rate` int(25) DEFAULT NULL,
  `min_refresh_rate` int(25) DEFAULT NULL,
  `monochrome` tinyint(1) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `number_color_planes` int(25) DEFAULT NULL,
  `number_video_pages` int(25) DEFAULT NULL,
  `pnp_device_id` varchar(90) DEFAULT NULL,
  `power_management_capabilities` int(25) DEFAULT NULL,
  `power_management_supported` tinyint(1) DEFAULT NULL,
  `protocol_supported` int(25) DEFAULT NULL,
  `reserved_system_palette_entries` int(25) DEFAULT NULL,
  `specification_version` int(25) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `status_info` varchar(45) DEFAULT NULL,
  `system_palette_entries` int(25) DEFAULT NULL,
  `time_last_reset` date DEFAULT NULL,
  `video_architecture` int(25) DEFAULT NULL,
  `video_memory_type` int(25) DEFAULT NULL,
  `video_mode` int(25) DEFAULT NULL,
  `video_mode_description` text,
  `video_processor` varchar(45) DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_hw_spec_video_session_data1_idx` (`session_data_id`),
  CONSTRAINT `fk_hw_spec_video_session_data1` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hw_spec_video`
--

LOCK TABLES `hw_spec_video` WRITE;
/*!40000 ALTER TABLE `hw_spec_video` DISABLE KEYS */;
INSERT INTO `hw_spec_video` VALUES (1,'0','NVIDIA','Integrated RAMDAC',1610612736,3,NULL,'NVIDIA GeForce GTX 580',0,0,'0','Win32_VideoController',32,2560,4294967296,0,0,59,4,1600,'NVIDIA GeForce GTX 580','VideoController1',0,0,'2013-09-12','9.18.13.2723',0,NULL,0,0,'oem35.inf','Section128',NULL,'nvd3dumx.dll,nvwgf2umx.dll,nvwgf2umx.dll,nvd3dum,nvwgf2um,nvwgf2um',0,0,0,75,56,0,'NVIDIA GeForce GTX 580',0,0,'PCI\\VEN_10DE&DEV_1080&SUBSYS_15803842&REV_A1\\4&2534E90F&0&0010',0,0,0,0,0,'OK','0',0,NULL,5,2,0,'2560 x 1600 x 4294967296 colors','GeForce GTX 580',1),(2,'0','NVIDIA','Integrated RAMDAC',1610612736,3,NULL,'NVIDIA GeForce GTX 580',0,0,'0','Win32_VideoController',32,2560,4294967296,0,0,59,4,1600,'NVIDIA GeForce GTX 580','VideoController1',0,0,'2013-09-12','9.18.13.2723',0,NULL,0,0,'oem35.inf','Section128',NULL,'nvd3dumx.dll,nvwgf2umx.dll,nvwgf2umx.dll,nvd3dum,nvwgf2um,nvwgf2um',0,0,0,75,56,0,'NVIDIA GeForce GTX 580',0,0,'PCI\\VEN_10DE&DEV_1080&SUBSYS_15803842&REV_A1\\4&2534E90F&0&0010',0,0,0,0,0,'OK','0',0,NULL,5,2,0,'2560 x 1600 x 4294967296 colors','GeForce GTX 580',2),(3,'0','NVIDIA','Integrated RAMDAC',1610612736,3,NULL,'NVIDIA GeForce GTX 580',0,0,'0','Win32_VideoController',32,2560,4294967296,0,0,59,4,1600,'NVIDIA GeForce GTX 580','VideoController1',0,0,'2013-09-12','9.18.13.2723',0,NULL,0,0,'oem35.inf','Section128',NULL,'nvd3dumx.dll,nvwgf2umx.dll,nvwgf2umx.dll,nvd3dum,nvwgf2um,nvwgf2um',0,0,0,75,56,0,'NVIDIA GeForce GTX 580',0,0,'PCI\\VEN_10DE&DEV_1080&SUBSYS_15803842&REV_A1\\4&2534E90F&0&0010',0,0,0,0,0,'OK','0',0,NULL,5,2,0,'2560 x 1600 x 4294967296 colors','GeForce GTX 580',3);
/*!40000 ALTER TABLE `hw_spec_video` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session_data`
--

DROP TABLE IF EXISTS `session_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(45) DEFAULT NULL,
  `addinVer` varchar(45) DEFAULT NULL,
  `revitBuild` varchar(45) DEFAULT NULL,
  `revitName` varchar(45) DEFAULT NULL,
  `revitLanguage` varchar(45) DEFAULT NULL,
  `session_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session_data`
--

LOCK TABLES `session_data` WRITE;
/*!40000 ALTER TABLE `session_data` DISABLE KEYS */;
INSERT INTO `session_data` VALUES (1,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(2,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(3,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(4,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(5,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(6,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(7,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(8,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab'),(9,'d.rudder@case-inc.com','v2013.10.14.0','20130709_2115(x64)','Autodesk Revit 2014','English_USA','f4ab4011-e4d7-466e-9581-d8510082feab');
/*!40000 ALTER TABLE `session_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timer_result`
--

DROP TABLE IF EXISTS `timer_result`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timer_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sequence` int(25) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `comment` text,
  `seconds` double DEFAULT NULL,
  `availPhysMem_start` double DEFAULT NULL,
  `availVirtMem_start` double DEFAULT NULL,
  `availPhysMem_end` double DEFAULT NULL,
  `availVirtMem_end` double DEFAULT NULL,
  `session_data_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_timer_result_session_data_idx` (`session_data_id`),
  CONSTRAINT `fk_timer_result_session_data` FOREIGN KEY (`session_data_id`) REFERENCES `session_data` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timer_result`
--

LOCK TABLES `timer_result` WRITE;
/*!40000 ALTER TABLE `timer_result` DISABLE KEYS */;
INSERT INTO `timer_result` VALUES (1,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,1),(2,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,1),(3,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,1),(4,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,1),(5,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,1),(6,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,1),(7,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,1),(8,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,1),(9,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,1),(10,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,1),(11,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,1),(12,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,1),(13,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,1),(14,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,1),(15,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,1),(16,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,1),(17,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,1),(18,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,1),(19,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,1),(20,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,1),(21,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,1),(22,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,1),(23,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,1),(24,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,1),(25,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,1),(26,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,1),(27,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,1),(28,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,1),(29,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,1),(30,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,1),(31,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,1),(32,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,1),(33,1,'Building(s) on Level: 1','',9.1,26785894400,8794227179520,26752823296,8794218889216,2),(34,2,'Building(s) on Level: 2','',17.5,26752823296,8794218889216,26737950720,8794198966272,2),(35,3,'Building(s) on Level: 3','',18,26737950720,8794198966272,26726481920,8794200539136,2),(36,4,'Building(s) on Level: 4','',18.4,26726481920,8794200539136,26716618752,8794167115776,2),(37,5,'Building(s) on Level: 5','',5.25,26716618752,8794167115776,26701713408,8794166067200,2),(38,6,'Building(s) on Level: 6','',9.6,26701713408,8794166067200,26685104128,8794150481920,2),(39,7,'Building(s) on Level: 7','',15.5,26685104128,8794150481920,26673725440,8794133704704,2),(40,8,'Building(s) on Level: 8','',21.2,26673725440,8794133704704,26668539904,8794119221248,2),(41,9,'Building(s) on Level: 9','',13.5,26668539904,8794119221248,26655006720,8794102444032,2),(42,10,'Building(s) on Level: 10','',9,26655006720,8794102444032,26647072768,8794085666816,2),(43,11,'Building(s) on Level: 11','',7.5,26647072768,8794085666816,26639835136,8794086699008,2),(44,12,'Building(s) on Level: 12','',14.5,26639835136,8794086699008,26627297280,8794052177920,2),(45,13,'Building(s) on Level: 13','',21,26627297280,8794052177920,26610167808,8794035400704,2),(46,14,'Building(s) on Level: 14','',5.8,26610167808,8794035400704,26591465472,8794018623488,2),(47,15,'Building(s) on Level: 15','',18.9,26591465472,8794018623488,26569977856,8794018410496,2),(48,16,'Building(s) on Level: 16','',15,26569977856,8794018410496,26553106432,8793985249280,2),(49,17,'Building(s) on Level: 17','',18,26553106432,8793985249280,26532081664,8793967423488,2),(50,18,'Building(s) on Level: 18','',23.5,26532081664,8793967423488,26511355904,8793934049280,2),(51,19,'Building(s) on Level: 19','',25,26511355904,8793934049280,26467753984,8793917272064,2),(52,20,'Building(s) on Level: 20','',18.8,26467753984,8793917272064,26464870400,8793900478464,2),(53,21,'Building(s) on Level: 21','',16.8,26464870400,8793900478464,26439323648,8793869217792,2),(54,22,'Building(s) on Level: 22','',14.5,26439323648,8793869217792,26418536448,8793852440576,2),(55,23,'Building(s) on Level: 23','',21,26418536448,8793852440576,26390831104,8793819066368,2),(56,24,'Building(s) on Level: 24','',23,26390831104,8793819066368,26363035648,8793802289152,2),(57,25,'Building(s) on Level: 25','',25,26363035648,8793802289152,26339442688,8793768914944,2),(58,26,'Create 1000 Detail Views','',94.5,26339274752,8793768652800,26312912896,8793751089152,2),(59,27,'Create 50 Plans and Sheets','',153.6,26312912896,8793751089152,25962700800,8793508687872,2),(60,28,'Save Model to Temp Directory','',8.5,25962700800,8793508687872,25993682944,8793462837248,2),(61,29,'Open Model from Temp','',9.5,25993682944,8793462837248,25984741376,8793526206464,2),(62,30,'Export DWF Files','',151.4,25984741376,8793526206464,0,0,2),(63,31,'Open 3D View','',138.4,25984188416,8793519955968,0,0,2),(64,32,'Adjust Types','',186.6,25985974272,8793549320192,0,0,2),(65,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,3),(66,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,3),(67,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,3),(68,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,3),(69,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,3),(70,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,3),(71,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,3),(72,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,3),(73,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,3),(74,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,3),(75,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,3),(76,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,3),(77,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,3),(78,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,3),(79,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,3),(80,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,3),(81,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,3),(82,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,3),(83,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,3),(84,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,3),(85,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,3),(86,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,3),(87,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,3),(88,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,3),(89,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,3),(90,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,3),(91,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,3),(92,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,3),(93,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,3),(94,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,3),(95,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,3),(96,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,3),(97,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,4),(98,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,4),(99,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,4),(100,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,4),(101,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,4),(102,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,4),(103,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,4),(104,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,4),(105,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,4),(106,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,4),(107,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,4),(108,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,4),(109,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,4),(110,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,4),(111,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,4),(112,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,4),(113,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,4),(114,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,4),(115,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,4),(116,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,4),(117,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,4),(118,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,4),(119,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,4),(120,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,4),(121,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,4),(122,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,4),(123,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,4),(124,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,4),(125,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,4),(126,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,4),(127,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,4),(128,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,4),(129,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,5),(130,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,5),(131,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,5),(132,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,5),(133,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,5),(134,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,5),(135,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,5),(136,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,5),(137,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,5),(138,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,5),(139,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,5),(140,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,5),(141,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,5),(142,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,5),(143,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,5),(144,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,5),(145,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,5),(146,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,5),(147,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,5),(148,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,5),(149,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,5),(150,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,5),(151,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,5),(152,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,5),(153,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,5),(154,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,5),(155,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,5),(156,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,5),(157,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,5),(158,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,5),(159,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,5),(160,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,5),(161,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,6),(162,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,6),(163,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,6),(164,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,6),(165,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,6),(166,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,6),(167,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,6),(168,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,6),(169,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,6),(170,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,6),(171,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,6),(172,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,6),(173,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,6),(174,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,6),(175,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,6),(176,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,6),(177,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,6),(178,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,6),(179,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,6),(180,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,6),(181,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,6),(182,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,6),(183,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,6),(184,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,6),(185,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,6),(186,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,6),(187,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,6),(188,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,6),(189,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,6),(190,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,6),(191,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,6),(192,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,6),(193,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,7),(194,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,7),(195,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,7),(196,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,7),(197,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,7),(198,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,7),(199,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,7),(200,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,7),(201,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,7),(202,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,7),(203,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,7),(204,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,7),(205,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,7),(206,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,7),(207,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,7),(208,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,7),(209,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,7),(210,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,7),(211,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,7),(212,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,7),(213,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,7),(214,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,7),(215,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,7),(216,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,7),(217,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,7),(218,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,7),(219,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,7),(220,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,7),(221,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,7),(222,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,7),(223,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,7),(224,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,7),(225,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,8),(226,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,8),(227,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,8),(228,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,8),(229,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,8),(230,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,8),(231,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,8),(232,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,8),(233,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,8),(234,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,8),(235,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,8),(236,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,8),(237,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,8),(238,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,8),(239,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,8),(240,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,8),(241,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,8),(242,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,8),(243,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,8),(244,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,8),(245,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,8),(246,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,8),(247,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,8),(248,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,8),(249,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,8),(250,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,8),(251,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,8),(252,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,8),(253,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,8),(254,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,8),(255,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,8),(256,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,8),(257,1,'Building(s) on Level: 1','',8.1,26785894400,8794227179520,26752823296,8794218889216,9),(258,2,'Building(s) on Level: 2','',7.5,26752823296,8794218889216,26737950720,8794198966272,9),(259,3,'Building(s) on Level: 3','',8,26737950720,8794198966272,26726481920,8794200539136,9),(260,4,'Building(s) on Level: 4','',8.4,26726481920,8794200539136,26716618752,8794167115776,9),(261,5,'Building(s) on Level: 5','',8.9,26716618752,8794167115776,26701713408,8794166067200,9),(262,6,'Building(s) on Level: 6','',9.5,26701713408,8794166067200,26685104128,8794150481920,9),(263,7,'Building(s) on Level: 7','',9.8,26685104128,8794150481920,26673725440,8794133704704,9),(264,8,'Building(s) on Level: 8','',10.3,26673725440,8794133704704,26668539904,8794119221248,9),(265,9,'Building(s) on Level: 9','',10.8,26668539904,8794119221248,26655006720,8794102444032,9),(266,10,'Building(s) on Level: 10','',11.3,26655006720,8794102444032,26647072768,8794085666816,9),(267,11,'Building(s) on Level: 11','',11.9,26647072768,8794085666816,26639835136,8794086699008,9),(268,12,'Building(s) on Level: 12','',12.6,26639835136,8794086699008,26627297280,8794052177920,9),(269,13,'Building(s) on Level: 13','',12.8,26627297280,8794052177920,26610167808,8794035400704,9),(270,14,'Building(s) on Level: 14','',13.3,26610167808,8794035400704,26591465472,8794018623488,9),(271,15,'Building(s) on Level: 15','',13.8,26591465472,8794018623488,26569977856,8794018410496,9),(272,16,'Building(s) on Level: 16','',14.4,26569977856,8794018410496,26553106432,8793985249280,9),(273,17,'Building(s) on Level: 17','',15.1,26553106432,8793985249280,26532081664,8793967423488,9),(274,18,'Building(s) on Level: 18','',15.4,26532081664,8793967423488,26511355904,8793934049280,9),(275,19,'Building(s) on Level: 19','',15.9,26511355904,8793934049280,26467753984,8793917272064,9),(276,20,'Building(s) on Level: 20','',16.4,26467753984,8793917272064,26464870400,8793900478464,9),(277,21,'Building(s) on Level: 21','',17.1,26464870400,8793900478464,26439323648,8793869217792,9),(278,22,'Building(s) on Level: 22','',17.4,26439323648,8793869217792,26418536448,8793852440576,9),(279,23,'Building(s) on Level: 23','',17.8,26418536448,8793852440576,26390831104,8793819066368,9),(280,24,'Building(s) on Level: 24','',18.4,26390831104,8793819066368,26363035648,8793802289152,9),(281,25,'Building(s) on Level: 25','',19.1,26363035648,8793802289152,26339442688,8793768914944,9),(282,26,'Create 1000 Detail Views','',71.3,26339274752,8793768652800,26312912896,8793751089152,9),(283,27,'Create 50 Plans and Sheets','',103.6,26312912896,8793751089152,25962700800,8793508687872,9),(284,28,'Save Model to Temp Directory','',6,25962700800,8793508687872,25993682944,8793462837248,9),(285,29,'Open Model from Temp','',6.9,25993682944,8793462837248,25984741376,8793526206464,9),(286,30,'Export DWF Files','',111.4,25984741376,8793526206464,0,0,9),(287,31,'Open 3D View','',108.4,25984188416,8793519955968,0,0,9),(288,32,'Adjust Types','',106.6,25985974272,8793549320192,0,0,9);
/*!40000 ALTER TABLE `timer_result` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-11-29 15:04:12
