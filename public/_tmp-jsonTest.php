<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Setup autoloading
require 'init_autoloader.php';

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

//composer
require 'vendor/autoload.php';
set_time_limit(0);


//$json=utf8_encode(file_Get_contents("public/BenchmarkData_rev.json"));
$json = '{"menu": {
  "id": "file",
  "value": false,
  "popup": {
    "menuitem": [
      {"value": "New", "onclick": "CreateNewDoc()"},
      {"value": "Open", "onclick": "OpenDoc()"},
      {"value": "Close", "onclick": "CloseDoc()"}
    ]
  }
}}';
// Decode JSON objects as PHP objects
$jsonData = Zend\Json\Json::decode($json, Zend\Json\Json::TYPE_OBJECT);

$result = get_object_vars($jsonData);


function convertBool(&$items){
	foreach ($items as $key=>$value){
	if($value==false){
		$items->$key = 0; 
		echo "wdi!";
	}elseif ($value==true){
		$items->$key = 1; 
	}
	}
}

array_walk_recursive($result,'convertBool');
var_dump ($jsonData);
echo "<br />";
var_dump ($result); 

echo "so: ".$jsonData->menu->value;
//echo "<br /> so2: ".$result['menu']['value'];