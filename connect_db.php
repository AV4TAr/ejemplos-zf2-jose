<?php 
	//composer
	require 'vendor/autoload.php';


	//http://framework.zend.com/manual/2.2/en/index.html#zend-db

	$configArray =array(
		'driver' => 'Mysqli',
		'database' => 'zend_db_example',
		'username' => 'root',
		'password' => 'root',
		'hostname' => '192.168.8.2'
	); 
	$adapter = new Zend\Db\Adapter\Adapter($configArray);
	
	$tableTest = new Zend\Db\TableGateway\TableGateway('test', $adapter);
	
	$tableTest->insert(array("nombre"=>"diego"));
	
	$registros = $TableTest->select(array('id' => 1));

	$registro = $registros->current();

	$registro->name = "jose";
	$registro->save();


	//Objecto ->  Zend\Db\TableGateway\TableGateway -> mapea una tabla
	//Select -> Coleccio de Zend\Db\RowGateway -> Zend\Db\ResultSet
	//Registro -> Zend\Db\RowGateway -> mapeo de un registro de la tabla